﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusGruposofertaLinb
    {
        public string Oferta { get; set; }
        public string Grupooferta { get; set; }
        public int? CodTipo { get; set; }
    }
}
