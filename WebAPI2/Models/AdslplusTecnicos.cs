﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusTecnicos
    {
        public AdslplusTecnicos()
        {
            AdslplusAprovAgendas = new HashSet<AdslplusAprovAgendas>();
            AdslplusAsegAgendas = new HashSet<AdslplusAsegAgendas>();
        }

        public Guid UserIdFk { get; set; }
        public string IdEmpleadoFk { get; set; }
        public string NoEtb { get; set; }
        public string CeluarPersonal { get; set; }
        public string CelularCorporativo { get; set; }
        public string TelefonoFijo { get; set; }
        public int? ZonaFk { get; set; }
        public string CodigoSecreto { get; set; }
        public string TipoTecnico { get; set; }

        public ICollection<AdslplusAprovAgendas> AdslplusAprovAgendas { get; set; }
        public ICollection<AdslplusAsegAgendas> AdslplusAsegAgendas { get; set; }
    }
}
