﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI2.Models
{
    public class DLActividades
    {
        public string Linea { get; set; }
        public string Solicitud { get; set; }
        public DateTime FechaAgenda { get; set; }
        public DateTime? FechaCierre { get; set; }
        public string Oferta { get; set; }
        public string TipoProducto { get; set; }
        public string Estado { get; set; }
        public string CodigoCierre { get; set; }
        public string IdTecnico { get; set; }
    }
}
