﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusSegmentos
    {
        public int IntIdOferta { get; set; }
        public string VarOferta { get; set; }
        public string VarTipo { get; set; }
        public string VarProducto { get; set; }
        public DateTime? DatFechaRegistro { get; set; }
        public bool? Activo { get; set; }
    }
}
