﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusAprovAgendas
    {
        public string LineaFk { get; set; }
        public string SolicitudFk { get; set; }
        public DateTime FechaAgendaFk { get; set; }
        public int CtivoAgenda { get; set; }
        public DateTime? FechaUltimaAgenda { get; set; }
        public string IdTecnicoFk { get; set; }
        public string Observaciones { get; set; }
        public int? Tipo { get; set; }
        public DateTime? FechaHoraRegistro { get; set; }
        public Guid? UsrRegistro { get; set; }
        public DateTime? FechaFinRegistro { get; set; }
        public Guid? UsrFinRegistro { get; set; }
        public DateTime? FechaHoraRegistro2 { get; set; }

        public AdslplusAprovisionamiento AdslplusAprovisionamiento { get; set; }
        public AdslplusTecnicos IdTecnicoFkNavigation { get; set; }
    }
}
