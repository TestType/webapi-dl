﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class NomEmpleados
    {
        public string IdEmpleado { get; set; }
        public string Apellidos { get; set; }
        public string Nombre { get; set; }
        public string Sexo { get; set; }
        public byte[] Foto { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Ciudad { get; set; }
        public string Tvivienda { get; set; }
        public string EstadoCiv { get; set; }
        public string Eps { get; set; }
        public string Arp { get; set; }
        public string FondoPen { get; set; }
        public string TallaZap { get; set; }
        public string TallaCam { get; set; }
        public string TallaOve { get; set; }
        public string Direccion { get; set; }
        public string Barrio { get; set; }
        public string Telefono { get; set; }
        public string NombreConyuge { get; set; }
        public int? Estatura { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
    }
}
