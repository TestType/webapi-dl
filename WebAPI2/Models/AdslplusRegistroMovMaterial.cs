﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusRegistroMovMaterial
    {
        public string NSolicitud { get; set; }
        public string Boleta { get; set; }
        public int CodTipMaterial { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime? FechaVencimiento { get; set; }
        public int CcSolicitante { get; set; }
        public string IdBodCiudadIni { get; set; }
        public string IdBodCiudadFin { get; set; }
        public string IdBodZonaIni { get; set; }
        public string IdBodZonaFin { get; set; }
        public int? IdTecnicoIni { get; set; }
        public int? IdTecnicoFin { get; set; }
        public int CodTipMov { get; set; }
        public string UnidadMedida { get; set; }
        public int Cantidad { get; set; }
        public string NFormImf04 { get; set; }
        public string ValeAcompañamiento { get; set; }
        public string CodColvatel { get; set; }
        public Guid? UsrRegistra { get; set; }
        public int? CodTipoUso { get; set; }
        public bool EnDerivacion { get; set; }
    }
}
