﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusTiposPqrs
    {
        public int IdTipologia { get; set; }
        public string Tipologia { get; set; }
    }
}
