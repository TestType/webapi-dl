﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class NomVigencias
    {
        public string IdVigencia { get; set; }
        public string Descripcion { get; set; }
        public DateTime? FechaIni { get; set; }
        public DateTime? FechaFin { get; set; }
        public int? Ctivo { get; set; }
    }
}
