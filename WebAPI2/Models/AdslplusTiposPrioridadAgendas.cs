﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusTiposPrioridadAgendas
    {
        public int IdTipoPrioridadAgenda { get; set; }
        public string TpoPrioridad { get; set; }
        public string Tpsigla { get; set; }
    }
}
