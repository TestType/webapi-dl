﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusArchivos
    {
        public int IdArchivo { get; set; }
        public string Nombre { get; set; }
        public byte[] Binario { get; set; }
        public Guid UserId { get; set; }
        public DateTime FechaCreacion { get; set; }

        public AspnetUsers User { get; set; }
    }
}
