﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusAseguramientoNogest
    {
        public string Linea { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Contacto { get; set; }
        public string Pri { get; set; }
        public DateTime? FechaReporte { get; set; }
        public string Antig { get; set; }
        public DateTime? FechaEnt { get; set; }
        public string Central { get; set; }
        public string ObservacionesMto { get; set; }
        public string Cola { get; set; }
        public string Zona { get; set; }
        public int? CodCierre { get; set; }
        public string SerieEquipo { get; set; }
        public string NombreRecibe { get; set; }
        public string Observaciones { get; set; }
        public string ObservacionesInconveniente { get; set; }
        public int CodTipo { get; set; }
        public string CodEstado { get; set; }
        public DateTime? FechaHoraCierre { get; set; }
        public DateTime FechaHoraVisita { get; set; }
        public string Distrito { get; set; }
        public string Liston { get; set; }
        public string ParL { get; set; }
        public string Secun { get; set; }
        public string ParS { get; set; }
        public string Pdatos { get; set; }
        public string Slot { get; set; }
        public string Serial { get; set; }
        public string Concentrador { get; set; }
        public string SerieEquipoRetirado { get; set; }
        public string PuertoDatos { get; set; }
        public string Pvoz { get; set; }
        public string RtaCod { get; set; }
        public string CfaCod { get; set; }
        public string LtaCod { get; set; }
        public string Ticket { get; set; }
        public int? Transporte { get; set; }
        public int? Gestion { get; set; }
        public int? Dg { get; set; }
        public int? FallaCpe { get; set; }
        public bool? CambioCpe { get; set; }
        public string Formatos { get; set; }
        public int? CantNeopren { get; set; }
        public Guid? UsrRegistra { get; set; }
        public string SerieEquipoInst { get; set; }
        public string ModeloEquipoRet { get; set; }
        public int? Alambre { get; set; }
        public int? Tensores { get; set; }
        public int? Tornillos { get; set; }
        public int? Grapas { get; set; }
        public int? Conectores { get; set; }
        public string ImagenGpnba { get; set; }
        public string CentralNew { get; set; }
        public string DistritoNew { get; set; }
        public string ListonNew { get; set; }
        public string ParLnew { get; set; }
        public string SecunNew { get; set; }
        public string ParSnew { get; set; }
        public string PvozNew { get; set; }
        public string EstadoContrato { get; set; }
    }
}
