﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusZonas
    {
        public int IdZona { get; set; }
        public string NombreZona { get; set; }
        public string IdBodega { get; set; }
    }
}
