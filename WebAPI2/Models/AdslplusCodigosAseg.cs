﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusCodigosAseg
    {
        public string Rtacod { get; set; }
        public string Cfacod { get; set; }
        public string Ltacod { get; set; }
        public string Rtades { get; set; }
        public string Cfades { get; set; }
        public string Ltades { get; set; }
        public bool? Activo { get; set; }
        public string TipoOfer { get; set; }
    }
}
