﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusProductoCodigo
    {
        public string CodTipProd { get; set; }
        public string DesTipProd { get; set; }
    }
}
