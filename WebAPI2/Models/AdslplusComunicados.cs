﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusComunicados
    {
        public DateTime FechaComunicado { get; set; }
        public string DestinoComunicado { get; set; }
        public int? TipoComunicado { get; set; }
        public string TituloComunicado { get; set; }
        public string Comunicado { get; set; }
        public DateTime? FechaFinComunicado { get; set; }
        public Guid? UsrRegistro { get; set; }
    }
}
