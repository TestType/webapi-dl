﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusEstadosDocumentacion
    {
        public AdslplusEstadosDocumentacion()
        {
            AdslplusDocumentacion = new HashSet<AdslplusDocumentacion>();
            AdslplusDocumentacionHistorico = new HashSet<AdslplusDocumentacionHistorico>();
        }

        public int IdEstadoDocumentacion { get; set; }
        public string Nombre { get; set; }

        public ICollection<AdslplusDocumentacion> AdslplusDocumentacion { get; set; }
        public ICollection<AdslplusDocumentacionHistorico> AdslplusDocumentacionHistorico { get; set; }
    }
}
