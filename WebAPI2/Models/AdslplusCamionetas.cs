﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusCamionetas
    {
        public string Placa { get; set; }
        public DateTime FechaDispo { get; set; }
        public string NombreConductor { get; set; }
        public string CelularConductor { get; set; }
        public Guid? UsrRegistra { get; set; }
    }
}
