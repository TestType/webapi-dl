﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusBodegaPrincipal
    {
        public string IdBodCiudad { get; set; }
        public string NomBodega { get; set; }
        public string Descripcion { get; set; }
        public bool? Visible { get; set; }
    }
}
