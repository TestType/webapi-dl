﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusRtaVillav
    {
        public int Idrta { get; set; }
        public string Rta { get; set; }
        public string RtaDesc { get; set; }
        public bool? Activo { get; set; }
    }
}
