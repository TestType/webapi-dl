﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebAPI2.Models
{
    public partial class ADSLplusContext : DbContext
    {
        public ADSLplusContext(DbContextOptions<ADSLplusContext> options): base(options) { }

        public virtual DbSet<AdslplusAprovAgendas> AdslplusAprovAgendas { get; set; }
        public virtual DbSet<AdslplusAprovisionamiento> AdslplusAprovisionamiento { get; set; }
        public virtual DbSet<AdslplusAprovisionamientoHis> AdslplusAprovisionamientoHis { get; set; }
        public virtual DbSet<AdslplusAprovisionamientoNogest> AdslplusAprovisionamientoNogest { get; set; }
        public virtual DbSet<AdslplusArchivos> AdslplusArchivos { get; set; }
        public virtual DbSet<AdslplusAsegAgendas> AdslplusAsegAgendas { get; set; }
        public virtual DbSet<AdslplusAseguramiento> AdslplusAseguramiento { get; set; }
        public virtual DbSet<AdslplusAseguramientoHis> AdslplusAseguramientoHis { get; set; }
        public virtual DbSet<AdslplusAseguramientoNogest> AdslplusAseguramientoNogest { get; set; }
        public virtual DbSet<AdslplusAuxtecnico> AdslplusAuxtecnico { get; set; }
        public virtual DbSet<AdslplusBodegaPrincipal> AdslplusBodegaPrincipal { get; set; }
        public virtual DbSet<AdslplusBodegas> AdslplusBodegas { get; set; }
        public virtual DbSet<AdslplusCamionetas> AdslplusCamionetas { get; set; }
        public virtual DbSet<AdslplusCfaVillav> AdslplusCfaVillav { get; set; }
        public virtual DbSet<AdslplusCodigosAprov> AdslplusCodigosAprov { get; set; }
        public virtual DbSet<AdslplusCodigosAseg> AdslplusCodigosAseg { get; set; }
        public virtual DbSet<AdslplusCodigosLinb> AdslplusCodigosLinb { get; set; }
        public virtual DbSet<AdslplusCodigosLogistica> AdslplusCodigosLogistica { get; set; }
        public virtual DbSet<AdslplusComunicados> AdslplusComunicados { get; set; }
        public virtual DbSet<AdslplusContratos> AdslplusContratos { get; set; }
        public virtual DbSet<AdslplusContratosHistoria> AdslplusContratosHistoria { get; set; }
        public virtual DbSet<AdslplusDocumentacion> AdslplusDocumentacion { get; set; }
        public virtual DbSet<AdslplusDocumentacionHistorico> AdslplusDocumentacionHistorico { get; set; }
        public virtual DbSet<AdslplusEquipos> AdslplusEquipos { get; set; }
        public virtual DbSet<AdslplusEquiposKardex> AdslplusEquiposKardex { get; set; }
        public virtual DbSet<AdslplusEstadosDocumentacion> AdslplusEstadosDocumentacion { get; set; }
        public virtual DbSet<AdslplusFallasCpe> AdslplusFallasCpe { get; set; }
        public virtual DbSet<AdslplusGruposofertaLinb> AdslplusGruposofertaLinb { get; set; }
        public virtual DbSet<AdslplusIdentificacionesExcluidas> AdslplusIdentificacionesExcluidas { get; set; }
        public virtual DbSet<AdslplusLfaVillav> AdslplusLfaVillav { get; set; }
        public virtual DbSet<AdslplusLineabAgendas> AdslplusLineabAgendas { get; set; }
        public virtual DbSet<AdslplusLineabAgendasBackUp> AdslplusLineabAgendasBackUp { get; set; }
        public virtual DbSet<AdslplusLineabasica> AdslplusLineabasica { get; set; }
        public virtual DbSet<AdslplusLineabasicaBackUp> AdslplusLineabasicaBackUp { get; set; }
        public virtual DbSet<AdslplusLineabasicaHist> AdslplusLineabasicaHist { get; set; }
        public virtual DbSet<AdslplusListaMaterial> AdslplusListaMaterial { get; set; }
        public virtual DbSet<AdslplusLogActualizacionEquipos> AdslplusLogActualizacionEquipos { get; set; }
        public virtual DbSet<AdslplusLogParameventos> AdslplusLogParameventos { get; set; }
        public virtual DbSet<AdslplusLogSolicitudes> AdslplusLogSolicitudes { get; set; }
        public virtual DbSet<AdslplusLogUsuarios> AdslplusLogUsuarios { get; set; }
        public virtual DbSet<AdslplusMarcasEquipos> AdslplusMarcasEquipos { get; set; }
        public virtual DbSet<AdslplusMaterialesKardex> AdslplusMaterialesKardex { get; set; }
        public virtual DbSet<AdslplusMaximosTecnico> AdslplusMaximosTecnico { get; set; }
        public virtual DbSet<AdslplusNovedad> AdslplusNovedad { get; set; }
        public virtual DbSet<AdslplusParameventos> AdslplusParameventos { get; set; }
        public virtual DbSet<AdslplusPqrs> AdslplusPqrs { get; set; }
        public virtual DbSet<AdslplusProdEquipos> AdslplusProdEquipos { get; set; }
        public virtual DbSet<AdslplusProdEquiposHis> AdslplusProdEquiposHis { get; set; }
        public virtual DbSet<AdslplusProdMaterial> AdslplusProdMaterial { get; set; }
        public virtual DbSet<AdslplusProductoCodigo> AdslplusProductoCodigo { get; set; }
        public virtual DbSet<AdslplusRegistroMovEquipo> AdslplusRegistroMovEquipo { get; set; }
        public virtual DbSet<AdslplusRegistroMovMaterial> AdslplusRegistroMovMaterial { get; set; }
        public virtual DbSet<AdslplusRtaVillav> AdslplusRtaVillav { get; set; }
        public virtual DbSet<AdslplusSegmentos> AdslplusSegmentos { get; set; }
        public virtual DbSet<AdslplusSolicitudes> AdslplusSolicitudes { get; set; }
        public virtual DbSet<AdslplusSolicitudesDetalle> AdslplusSolicitudesDetalle { get; set; }
        public virtual DbSet<AdslplusSolicitudHistoria> AdslplusSolicitudHistoria { get; set; }
        public virtual DbSet<AdslplusTecnicos> AdslplusTecnicos { get; set; }
        public virtual DbSet<AdslplusTipoMovimientos> AdslplusTipoMovimientos { get; set; }
        public virtual DbSet<AdslplusTiposAgendas> AdslplusTiposAgendas { get; set; }
        public virtual DbSet<AdslplusTiposCausaDevolucion> AdslplusTiposCausaDevolucion { get; set; }
        public virtual DbSet<AdslplusTiposDocumentacion> AdslplusTiposDocumentacion { get; set; }
        public virtual DbSet<AdslplusTiposNovedad> AdslplusTiposNovedad { get; set; }
        public virtual DbSet<AdslplusTiposOferta> AdslplusTiposOferta { get; set; }
        public virtual DbSet<AdslplusTiposPqrs> AdslplusTiposPqrs { get; set; }
        public virtual DbSet<AdslplusTiposPrioridadAgendas> AdslplusTiposPrioridadAgendas { get; set; }
        public virtual DbSet<AdslplusTransferencia> AdslplusTransferencia { get; set; }
        public virtual DbSet<AdslplusTransferenciaDetalle> AdslplusTransferenciaDetalle { get; set; }
        public virtual DbSet<AdslplusTransportes> AdslplusTransportes { get; set; }
        public virtual DbSet<AdslplusUsuarios> AdslplusUsuarios { get; set; }
        public virtual DbSet<AdslplusUsuariosCoordinaciones> AdslplusUsuariosCoordinaciones { get; set; }
        public virtual DbSet<AdslplusZonas> AdslplusZonas { get; set; }
        public virtual DbSet<AspnetApplications> AspnetApplications { get; set; }
        public virtual DbSet<AspnetMembership> AspnetMembership { get; set; }
        public virtual DbSet<AspnetPaths> AspnetPaths { get; set; }
        public virtual DbSet<AspnetPersonalizationAllUsers> AspnetPersonalizationAllUsers { get; set; }
        public virtual DbSet<AspnetPersonalizationPerUser> AspnetPersonalizationPerUser { get; set; }
        public virtual DbSet<AspnetProfile> AspnetProfile { get; set; }
        public virtual DbSet<AspnetRoles> AspnetRoles { get; set; }
        public virtual DbSet<AspnetSchemaVersions> AspnetSchemaVersions { get; set; }
        public virtual DbSet<AspnetUsers> AspnetUsers { get; set; }
        public virtual DbSet<AspnetUsersInRoles> AspnetUsersInRoles { get; set; }
        public virtual DbSet<AspnetWebEventEvents> AspnetWebEventEvents { get; set; }
        public virtual DbSet<CodigosDevolucion> CodigosDevolucion { get; set; }
        public virtual DbSet<Devoluciones> Devoluciones { get; set; }
        public virtual DbSet<GestionAdsl> GestionAdsl { get; set; }
        public virtual DbSet<GestionLb> GestionLb { get; set; }
        public virtual DbSet<Logsito> Logsito { get; set; }
        public virtual DbSet<NomEmpleados> NomEmpleados { get; set; }
        public virtual DbSet<NomEmpleados2> NomEmpleados2 { get; set; }
        public virtual DbSet<NomVigencias> NomVigencias { get; set; }
        public virtual DbSet<TempAdslplusProdEquipos> TempAdslplusProdEquipos { get; set; }
        public virtual DbSet<TempEmpleados> TempEmpleados { get; set; }

        // Unable to generate entity type for table 'dbo.temp_error_aprov'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.temp_lineas'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.temp_solicitudes'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TEMP_TECNICOS'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ADSLPLUS_LINEA_USO'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TEMP_ZONAS'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TEMPSERIALES'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Tienda_Informacion'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ADSLPLUS_LINEABASICA_NOGEST'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ADSLPLUS_LISTA_EQUIPOS'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ADSLPLUS_PROD_ESTADO'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ADSLPLUS_TIPOS_PRODUCTOS'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ADSLPLUS_ASEG_TEMPORAL'. Please see the warning messages.

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AdslplusAprovAgendas>(entity =>
            {
                entity.HasKey(e => new { e.LineaFk, e.SolicitudFk, e.FechaAgendaFk, e.CtivoAgenda });

                entity.ToTable("ADSLPLUS_APROV_AGENDAS");

                entity.HasIndex(e => new { e.SolicitudFk, e.IdTecnicoFk })
                    .HasName("<Name of Missing Index, sysname,>");

                entity.Property(e => e.LineaFk)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.SolicitudFk)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.FechaAgendaFk).HasColumnType("datetime");

                entity.Property(e => e.FechaFinRegistro).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraRegistro).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraRegistro2).HasColumnType("datetime");

                entity.Property(e => e.FechaUltimaAgenda).HasColumnType("datetime");

                entity.Property(e => e.IdTecnicoFk)
                    .HasColumnName("IdTecnicoFK")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdTecnicoFkNavigation)
                    .WithMany(p => p.AdslplusAprovAgendas)
                    .HasForeignKey(d => d.IdTecnicoFk)
                    .HasConstraintName("FK_ADSLPLUS_APROV_AGENDAS_ADSLPLUS_TECNICOS");

                entity.HasOne(d => d.AdslplusAprovisionamiento)
                    .WithMany(p => p.AdslplusAprovAgendas)
                    .HasForeignKey(d => new { d.LineaFk, d.SolicitudFk, d.FechaAgendaFk })
                    .HasConstraintName("FK_ADSLPLUS_APROV_AGENDAS_ADSLPLUS_APROVISIONAMIENTO");
            });

            modelBuilder.Entity<AdslplusAprovisionamiento>(entity =>
            {
                entity.HasKey(e => new { e.Linea, e.Solicitud, e.FechaAgenda });

                entity.ToTable("ADSLPLUS_APROVISIONAMIENTO");

                entity.HasIndex(e => new { e.Linea, e.Solicitud, e.Oferta, e.Identificacion, e.Detalle, e.Coordinacion, e.FechaAgenda })
                    .HasName("ADSLPLUS_APROVISIONAMIENTO_Indice1");

                entity.HasIndex(e => new { e.AlambreCx, e.AutoSoportado, e.Cliente, e.CodEstado, e.CodigoCierre, e.Conectores, e.Detalle, e.Distrito, e.Equipo, e.EquipoRetirado, e.FechaAgenda, e.FechaCierre, e.Grapas, e.HoraInicio, e.Linea, e.Liston, e.Neopren, e.NombreTecnico, e.NoMicroFiltros, e.Observaciones, e.Oferta, e.ParL, e.ParS, e.Pdatos, e.PrimerUso, e.Secun, e.SerialVoip, e.SerieEquipo, e.Solicitud, e.Tensores, e.Ticket, e.TipoProducto, e.Tornillos, e.TipoInstala })
                    .HasName("nci_wi_ADSLPLUS_APROVISIONAMIENTO_BFC7FDE4DB63C03B0110");

                entity.Property(e => e.Linea)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Solicitud)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.FechaAgenda).HasColumnType("datetime");

                entity.Property(e => e.AlambreCx).HasColumnName("AlambreCX");

                entity.Property(e => e.AlianzaDirectv)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Atenuacion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CanalVenta)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Central)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Central1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CentralAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Clausula)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Cliente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodEstado)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoCierre)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Contacto)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Contratista)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Contrato)
                    .HasColumnName("CONTRATO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Coordinacion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Detalle)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Distrito)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DistritoAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Equipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EquipoRetirado)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoContrato)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Etbtecnico)
                    .HasColumnName("ETBtecnico")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaCierre).HasColumnType("datetime");

                entity.Property(e => e.FechaSolicitud).HasColumnType("datetime");

                entity.Property(e => e.HoraInicio)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.IdArchivoGpbna).HasColumnName("IdArchivoGPBNA");

                entity.Property(e => e.IdArchivoRom).HasColumnName("IdArchivoROM");

                entity.Property(e => e.Identificacion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ImagenGpnba)
                    .HasColumnName("imagenGPNBA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Impresa)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ip1)
                    .HasColumnName("IP1")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Ip2)
                    .HasColumnName("IP2")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Ip3)
                    .HasColumnName("IP3")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Ip4)
                    .HasColumnName("IP4")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Iplan)
                    .HasColumnName("IPLAN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Iplanrouter)
                    .HasColumnName("IPLANRouter")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ipwanlocal)
                    .HasColumnName("IPWANLocal")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ipwanremota)
                    .HasColumnName("IPWANRemota")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Liston)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ListonAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MarcaEquipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModoBridge)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NoPcs).HasColumnName("NoPCs");

                entity.Property(e => e.NoPcsInst).HasColumnName("NoPCsInst");

                entity.Property(e => e.NombreRecibe)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreTecnico)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumContrato)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ObsInconvenientes)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.Oferta)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParL)
                    .HasColumnName("Par_L")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParListonAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParS)
                    .HasColumnName("Par_S")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParSecunAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pdatos)
                    .HasColumnName("PDatos")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PlanDesmonte)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PosDatosAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PosVozAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PosicionAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrimerUso)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Prioridad)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PrioridadEtb)
                    .HasColumnName("PrioridadETB")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Promocion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReqRedLan)
                    .HasColumnName("ReqRedLAN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RevisionTecnica)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Secun)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SecundariaAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerialVoip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Slot)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Snr)
                    .HasColumnName("SNR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Soperativo)
                    .HasColumnName("SOperativo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TDg).HasColumnName("T_DG");

                entity.Property(e => e.TGestion).HasColumnName("T_Gestion");

                entity.Property(e => e.TTransporte).HasColumnName("T_Transporte");

                entity.Property(e => e.TelContacto)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ticket)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoProducto)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UrbaAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Usuario)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vci)
                    .HasColumnName("VCI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vciusuario)
                    .HasColumnName("VCIUsuario")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vpi)
                    .HasColumnName("VPI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vpiusuario)
                    .HasColumnName("VPIUsuario")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ZonaInstalador)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusAprovisionamientoHis>(entity =>
            {
                entity.HasKey(e => new { e.Linea, e.Solicitud, e.FechaAgenda, e.FechaHoraReact });

                entity.ToTable("ADSLPLUS_APROVISIONAMIENTO_HIS");

                entity.Property(e => e.Linea)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Solicitud)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.FechaAgenda).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraReact).HasColumnType("datetime");

                entity.Property(e => e.AlambreCx).HasColumnName("AlambreCX");

                entity.Property(e => e.AlianzaDirectv)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Atenuacion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Central)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Central1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CentralAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Clausula)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Cliente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodEstado)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoCierre)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Contacto)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Contratista)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Coordinacion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Detalle)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Distrito)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DistritoAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Equipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EquipoRetirado)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoContrato)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Etbtecnico)
                    .HasColumnName("ETBtecnico")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaCierre).HasColumnType("datetime");

                entity.Property(e => e.FechaSolicitud).HasColumnType("datetime");

                entity.Property(e => e.HoraInicio)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Identificacion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ImagenGpnba)
                    .HasColumnName("imagenGPNBA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Impresa)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ip1)
                    .HasColumnName("IP1")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Ip2)
                    .HasColumnName("IP2")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Ip3)
                    .HasColumnName("IP3")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Ip4)
                    .HasColumnName("IP4")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Iplan)
                    .HasColumnName("IPLAN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Iplanrouter)
                    .HasColumnName("IPLANRouter")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ipwanlocal)
                    .HasColumnName("IPWANLocal")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ipwanremota)
                    .HasColumnName("IPWANRemota")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Liston)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ListonAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MarcaEquipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModoBridge)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NoPcs).HasColumnName("NoPCs");

                entity.Property(e => e.NoPcsInst).HasColumnName("NoPCsInst");

                entity.Property(e => e.NombreRecibe)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreTecnico)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumContrato)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ObsInconvenientes)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Oferta)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParL)
                    .HasColumnName("Par_L")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParListonAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParS)
                    .HasColumnName("Par_S")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParSecunAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pdatos)
                    .HasColumnName("PDatos")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PlanDesmonte)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PosDatosAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PosVozAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PosicionAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrimerUso)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PrioridadEtb)
                    .HasColumnName("PrioridadETB")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Promocion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReqRedLan)
                    .HasColumnName("ReqRedLAN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RevisionTecnica)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Secun)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SecundariaAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerialVoip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Slot)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Snr)
                    .HasColumnName("SNR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Soperativo)
                    .HasColumnName("SOperativo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TDg).HasColumnName("T_DG");

                entity.Property(e => e.TGestion).HasColumnName("T_Gestion");

                entity.Property(e => e.TTransporte).HasColumnName("T_Transporte");

                entity.Property(e => e.TelContacto)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ticket)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoProducto)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UrbaAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Usuario)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vci)
                    .HasColumnName("VCI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vciusuario)
                    .HasColumnName("VCIUsuario")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vpi)
                    .HasColumnName("VPI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vpiusuario)
                    .HasColumnName("VPIUsuario")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ZonaInstalador)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusAprovisionamientoNogest>(entity =>
            {
                entity.HasKey(e => new { e.Linea, e.Solicitud, e.FechaAgenda });

                entity.ToTable("ADSLPLUS_APROVISIONAMIENTO_NOGEST");

                entity.Property(e => e.Linea)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Solicitud)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.FechaAgenda).HasColumnType("datetime");

                entity.Property(e => e.AlambreCx).HasColumnName("AlambreCX");

                entity.Property(e => e.AlianzaDirectv)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Atenuacion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Central)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Central1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CentralAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Clausula)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Cliente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodEstado)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoCierre)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Contacto)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Contratista)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Coordinacion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Detalle)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Distrito)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DistritoAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Equipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EquipoRetirado)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoContrato)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Etbtecnico)
                    .HasColumnName("ETBtecnico")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaCierre).HasColumnType("datetime");

                entity.Property(e => e.FechaSolicitud).HasColumnType("datetime");

                entity.Property(e => e.HoraInicio)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Identificacion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ImagenGpnba)
                    .HasColumnName("imagenGPNBA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Impresa)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ip1)
                    .HasColumnName("IP1")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Ip2)
                    .HasColumnName("IP2")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Ip3)
                    .HasColumnName("IP3")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Ip4)
                    .HasColumnName("IP4")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Iplan)
                    .HasColumnName("IPLAN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Iplanrouter)
                    .HasColumnName("IPLANRouter")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ipwanlocal)
                    .HasColumnName("IPWANLocal")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ipwanremota)
                    .HasColumnName("IPWANRemota")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Liston)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ListonAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MarcaEquipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModoBridge)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NoPcs).HasColumnName("NoPCs");

                entity.Property(e => e.NoPcsInst).HasColumnName("NoPCsInst");

                entity.Property(e => e.NombreRecibe)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreTecnico)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumContrato)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ObsInconvenientes)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Oferta)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParL)
                    .HasColumnName("Par_L")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParListonAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParS)
                    .HasColumnName("Par_S")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParSecunAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pdatos)
                    .HasColumnName("PDatos")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PlanDesmonte)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PosDatosAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PosVozAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PosicionAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrimerUso)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PrioridadEtb)
                    .HasColumnName("PrioridadETB")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Promocion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReqRedLan)
                    .HasColumnName("ReqRedLAN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RevisionTecnica)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Secun)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SecundariaAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerialVoip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Slot)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Snr)
                    .HasColumnName("SNR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Soperativo)
                    .HasColumnName("SOperativo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TDg).HasColumnName("T_DG");

                entity.Property(e => e.TGestion).HasColumnName("T_Gestion");

                entity.Property(e => e.TTransporte).HasColumnName("T_Transporte");

                entity.Property(e => e.TelContacto)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ticket)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoProducto)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UrbaAsg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Usuario)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vci)
                    .HasColumnName("VCI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vciusuario)
                    .HasColumnName("VCIUsuario")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vpi)
                    .HasColumnName("VPI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vpiusuario)
                    .HasColumnName("VPIUsuario")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ZonaInstalador)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusArchivos>(entity =>
            {
                entity.HasKey(e => e.IdArchivo)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("ADSLPLUS_ARCHIVOS");

                entity.HasIndex(e => e.IdArchivo)
                    .HasName("Index_ADSLPLUS_ARCHIVOS")
                    .IsUnique()
                    .ForSqlServerIsClustered();

                entity.Property(e => e.Binario).IsRequired();

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AdslplusArchivos)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ArchivosUsers");
            });

            modelBuilder.Entity<AdslplusAsegAgendas>(entity =>
            {
                entity.HasKey(e => new { e.LineaFk, e.FechaAgendaFk, e.CtivoAgenda });

                entity.ToTable("ADSLPLUS_ASEG_AGENDAS");

                entity.Property(e => e.LineaFk)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.FechaAgendaFk).HasColumnType("datetime");

                entity.Property(e => e.FechaFinRegistro).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraRegistro).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraRegistro2).HasColumnType("datetime");

                entity.Property(e => e.FechaUltimaAgenda).HasColumnType("datetime");

                entity.Property(e => e.IdTecnicoFk)
                    .HasColumnName("IdTecnicoFK")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdTecnicoFkNavigation)
                    .WithMany(p => p.AdslplusAsegAgendas)
                    .HasForeignKey(d => d.IdTecnicoFk)
                    .HasConstraintName("FK_ADSLPLUS_ASEG_AGENDAS_ADSLPLUS_TECNICOS1");

                entity.HasOne(d => d.AdslplusAseguramiento)
                    .WithMany(p => p.AdslplusAsegAgendas)
                    .HasForeignKey(d => new { d.LineaFk, d.FechaAgendaFk })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ADSLPLUS_ASEG_AGENDAS_ADSLPLUS_ASEGURAMIENTO");
            });

            modelBuilder.Entity<AdslplusAseguramiento>(entity =>
            {
                entity.HasKey(e => new { e.Linea, e.FechaHoraVisita });

                entity.ToTable("ADSLPLUS_ASEGURAMIENTO");

                entity.HasIndex(e => e.FechaHoraVisita)
                    .HasName("aseguramiento_fechaVisita");

                entity.HasIndex(e => new { e.CodTipo, e.FechaHoraVisita, e.SerieEquipoRetirado })
                    .HasName("nci_wi_ADSLPLUS_ASEGURAMIENTO_86906C69-F54D-4582-AF13-C07C874703BA");

                entity.HasIndex(e => new { e.Linea, e.FechaHoraVisita, e.CodEstado })
                    .HasName("aseguramineto_CodEstado");

                entity.Property(e => e.Linea)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHoraVisita).HasColumnType("datetime");

                entity.Property(e => e.AdapterCpe).HasColumnName("AdapterCPE");

                entity.Property(e => e.AlambreBajadaLb).HasColumnName("AlambreBajadaLB");

                entity.Property(e => e.AlambrePaseLb).HasColumnName("AlambrePaseLB");

                entity.Property(e => e.AlambreacometidaLb).HasColumnName("alambreacometidaLB");

                entity.Property(e => e.Antig)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CambioCpe).HasColumnName("CambioCPE");

                entity.Property(e => e.CcierreRe).HasColumnName("CCierreRE");

                entity.Property(e => e.Central)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CentralNew)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CfaCod)
                    .HasColumnName("CFA_Cod")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CodCierreRe).HasColumnName("COD_CIERRE_RE");

                entity.Property(e => e.CodEstado)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Cola)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Concentrador)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ConectorEmpalmeLb).HasColumnName("ConectorEmpalmeLB");

                entity.Property(e => e.ConectorRellLb).HasColumnName("ConectorRellLB");

                entity.Property(e => e.Contacto)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Dg).HasColumnName("DG");

                entity.Property(e => e.Direccion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Distrito)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DistritoNew)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoContrato)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FallaCpe).HasColumnName("FallaCPE");

                entity.Property(e => e.FechaEnt).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraCierre).HasColumnType("datetime");

                entity.Property(e => e.FechaReporte).HasColumnType("datetime");

                entity.Property(e => e.Formatos)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IdArchivoRom).HasColumnName("IdArchivoROM");

                entity.Property(e => e.IdSolicitudRe).HasColumnName("ID_SOLICITUD_RE");

                entity.Property(e => e.IdSolicitudRe1).HasColumnName("IdSolicitudRE");

                entity.Property(e => e.ImagenGpnba)
                    .HasColumnName("imagenGPNBA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Liston)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ListonNew)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LoginEquipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LtaCod)
                    .HasColumnName("LTA_Cod")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MacEquipoInst)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.MicrofiltroLb).HasColumnName("MicrofiltroLB");

                entity.Property(e => e.ModeloEquipoRet)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreRecibe)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.ObservacionesInconveniente)
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.ObservacionesMto)
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.ParL)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParLnew)
                    .HasColumnName("ParLNew")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParS)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParSnew)
                    .HasColumnName("ParSNew")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordEquipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pdatos)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pri)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PuertoDatos)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pvoz)
                    .HasColumnName("PVoz")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PvozNew)
                    .HasColumnName("PVozNew")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RtaCod)
                    .HasColumnName("RTA_Cod")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Secun)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SecunNew)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Serial)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipoInst)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipoRetirado)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Slot)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TensorLb).HasColumnName("TensorLB");

                entity.Property(e => e.Ticket)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.UsuarioEquipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zona)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusAseguramientoHis>(entity =>
            {
                entity.HasKey(e => new { e.Linea, e.FechaHoraVisita, e.FechaHoraReact });

                entity.ToTable("ADSLPLUS_ASEGURAMIENTO_HIS");

                entity.Property(e => e.Linea)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHoraVisita).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraReact).HasColumnType("datetime");

                entity.Property(e => e.Antig)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CambioCpe).HasColumnName("CambioCPE");

                entity.Property(e => e.Central)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CentralNew)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CfaCod)
                    .HasColumnName("CFA_Cod")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CodEstado)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Cola)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Concentrador)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Contacto)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Dg).HasColumnName("DG");

                entity.Property(e => e.Direccion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Distrito)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DistritoNew)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoContrato)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FallaCpe).HasColumnName("FallaCPE");

                entity.Property(e => e.FechaEnt).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraCierre).HasColumnType("datetime");

                entity.Property(e => e.FechaReporte).HasColumnType("datetime");

                entity.Property(e => e.Formatos)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ImagenGpnba)
                    .HasColumnName("imagenGPNBA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Liston)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ListonNew)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LtaCod)
                    .HasColumnName("LTA_Cod")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MacEquipoInst)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ModeloEquipoRet)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreRecibe)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.ObservacionesInconveniente)
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.ObservacionesMto)
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.ParL)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParLnew)
                    .HasColumnName("ParLNew")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParS)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParSnew)
                    .HasColumnName("ParSNew")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pdatos)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pri)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PuertoDatos)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pvoz)
                    .HasColumnName("PVoz")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PvozNew)
                    .HasColumnName("PVozNew")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RtaCod)
                    .HasColumnName("RTA_Cod")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Secun)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SecunNew)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Serial)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipoInst)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipoRetirado)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Slot)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ticket)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Zona)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusAseguramientoNogest>(entity =>
            {
                entity.HasKey(e => new { e.Linea, e.FechaHoraVisita });

                entity.ToTable("ADSLPLUS_ASEGURAMIENTO_NOGEST");

                entity.Property(e => e.Linea)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHoraVisita).HasColumnType("datetime");

                entity.Property(e => e.Antig)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CambioCpe).HasColumnName("CambioCPE");

                entity.Property(e => e.Central)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CentralNew)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CfaCod)
                    .HasColumnName("CFA_Cod")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CodEstado)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Cola)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Concentrador)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Contacto)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Dg).HasColumnName("DG");

                entity.Property(e => e.Direccion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Distrito)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DistritoNew)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoContrato)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FallaCpe).HasColumnName("FallaCPE");

                entity.Property(e => e.FechaEnt).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraCierre).HasColumnType("datetime");

                entity.Property(e => e.FechaReporte).HasColumnType("datetime");

                entity.Property(e => e.Formatos)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ImagenGpnba)
                    .HasColumnName("imagenGPNBA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Liston)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ListonNew)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LtaCod)
                    .HasColumnName("LTA_Cod")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ModeloEquipoRet)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreRecibe)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.ObservacionesInconveniente)
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.ObservacionesMto)
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.ParL)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParLnew)
                    .HasColumnName("ParLNew")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParS)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParSnew)
                    .HasColumnName("ParSNew")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pdatos)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pri)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PuertoDatos)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pvoz)
                    .HasColumnName("PVoz")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PvozNew)
                    .HasColumnName("PVozNew")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RtaCod)
                    .HasColumnName("RTA_Cod")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Secun)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SecunNew)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Serial)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipoInst)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipoRetirado)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Slot)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ticket)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Zona)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusAuxtecnico>(entity =>
            {
                entity.HasKey(e => e.IdTecnicoAux);

                entity.ToTable("ADSLPLUS_AUXTECNICO");

                entity.Property(e => e.Cargo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Contratante)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FechaRegistro).HasColumnType("datetime");

                entity.Property(e => e.Identificacion)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusBodegaPrincipal>(entity =>
            {
                entity.HasKey(e => e.IdBodCiudad);

                entity.ToTable("ADSLPLUS_BODEGA_PRINCIPAL");

                entity.Property(e => e.IdBodCiudad)
                    .HasColumnName("Id_BodCiudad")
                    .HasMaxLength(15)
                    .ValueGeneratedNever();

                entity.Property(e => e.Descripcion).HasMaxLength(200);

                entity.Property(e => e.NomBodega)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<AdslplusBodegas>(entity =>
            {
                entity.HasKey(e => e.Bodega);

                entity.ToTable("ADSLPLUS_BODEGAS");

                entity.Property(e => e.Bodega)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<AdslplusCamionetas>(entity =>
            {
                entity.HasKey(e => new { e.Placa, e.FechaDispo });

                entity.ToTable("ADSLPLUS_CAMIONETAS");

                entity.Property(e => e.Placa)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.FechaDispo).HasColumnType("datetime");

                entity.Property(e => e.CelularConductor)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreConductor)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusCfaVillav>(entity =>
            {
                entity.HasKey(e => e.IdCfa);

                entity.ToTable("ADSLPLUS_CFA_VILLAV");

                entity.Property(e => e.IdCfa).HasColumnName("idCFA");

                entity.Property(e => e.Cfa)
                    .IsRequired()
                    .HasColumnName("CFA")
                    .HasMaxLength(50);

                entity.Property(e => e.CfaDesc)
                    .IsRequired()
                    .HasColumnName("CFA_Desc")
                    .HasMaxLength(200);

                entity.Property(e => e.CodRta)
                    .IsRequired()
                    .HasColumnName("codRTA")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<AdslplusCodigosAprov>(entity =>
            {
                entity.HasKey(e => new { e.IdCodigo, e.TipoCodigo });

                entity.ToTable("ADSLPLUS_CODIGOS_APROV");

                entity.Property(e => e.IdCodigo)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DescripcionCodigo)
                    .HasMaxLength(70)
                    .IsUnicode(false);

                entity.Property(e => e.Naturaleza)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Origen)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusCodigosAseg>(entity =>
            {
                entity.HasKey(e => new { e.Rtacod, e.Cfacod, e.Ltacod });

                entity.ToTable("ADSLPLUS_CODIGOS_ASEG");

                entity.Property(e => e.Rtacod)
                    .HasColumnName("RTAcod")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Cfacod)
                    .HasColumnName("CFAcod")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Ltacod)
                    .HasColumnName("LTAcod")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Cfades)
                    .HasColumnName("CFAdes")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ltades)
                    .HasColumnName("LTAdes")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Rtades)
                    .HasColumnName("RTAdes")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TipoOfer)
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusCodigosLinb>(entity =>
            {
                entity.HasKey(e => new { e.Rtacod, e.Cfacod, e.Ltacod });

                entity.ToTable("ADSLPLUS_CODIGOS_LINB");

                entity.Property(e => e.Rtacod)
                    .HasColumnName("RTAcod")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Cfacod)
                    .HasColumnName("CFAcod")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Ltacod)
                    .HasColumnName("LTAcod")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Cfades)
                    .HasColumnName("CFAdes")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ltades)
                    .HasColumnName("LTAdes")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Rtades)
                    .HasColumnName("RTAdes")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusCodigosLogistica>(entity =>
            {
                entity.HasKey(e => e.Codigo);

                entity.ToTable("ADSLPLUS_CODIGOS_LOGISTICA");

                entity.Property(e => e.Codigo)
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UnidadMedida)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusComunicados>(entity =>
            {
                entity.HasKey(e => new { e.FechaComunicado, e.DestinoComunicado });

                entity.ToTable("ADSLPLUS_COMUNICADOS");

                entity.Property(e => e.FechaComunicado).HasColumnType("datetime");

                entity.Property(e => e.DestinoComunicado)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Comunicado)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFinComunicado).HasColumnType("datetime");

                entity.Property(e => e.TituloComunicado)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusContratos>(entity =>
            {
                entity.HasKey(e => e.NumContrato);

                entity.ToTable("ADSLPLUS_CONTRATOS");

                entity.Property(e => e.NumContrato)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.FechaRecibe).HasColumnType("datetime");

                entity.Property(e => e.IdTecnicoFk)
                    .HasColumnName("IdTecnicoFK")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusContratosHistoria>(entity =>
            {
                entity.HasKey(e => new { e.NumContrato, e.FechaEstado });

                entity.ToTable("ADSLPLUS_CONTRATOS_HISTORIA");

                entity.Property(e => e.NumContrato)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FechaEstado).HasColumnType("datetime");

                entity.Property(e => e.Estado)
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusDocumentacion>(entity =>
            {
                entity.HasKey(e => e.IdDocumentacion);

                entity.ToTable("ADSLPLUS_DOCUMENTACION");

                entity.HasIndex(e => new { e.AprovisionamientoFechaAgenda, e.AprovisionamientoLinea, e.AprovisionamientoSolicitud, e.LineaBasicaDetalle, e.LineaBasicaFecha, e.LineaBasicaSolicitud, e.IdTipoDocumentacion, e.IdEstadoDocumentacion })
                    .HasName("nci_wi_ADSLPLUS_DOCUMENTACION_4ACE26566EB90892C5BD0617E07B4806");

                entity.Property(e => e.AprovisionamientoFechaAgenda).HasColumnType("datetime");

                entity.Property(e => e.AprovisionamientoLinea)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.AprovisionamientoSolicitud)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.LineaBasicaDetalle)
                    .HasColumnName("LineaBasicaDETALLE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LineaBasicaFecha)
                    .HasColumnName("LineaBasicaFECHA")
                    .HasColumnType("datetime");

                entity.Property(e => e.LineaBasicaSolicitud)
                    .HasColumnName("LineaBasicaSOLICITUD")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.LineaCorrecta)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.SolicitudCorrecta)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdEstadoDocumentacionNavigation)
                    .WithMany(p => p.AdslplusDocumentacion)
                    .HasForeignKey(d => d.IdEstadoDocumentacion)
                    .HasConstraintName("Relationship1");

                entity.HasOne(d => d.IdTipoCausaDevolucionNavigation)
                    .WithMany(p => p.AdslplusDocumentacion)
                    .HasForeignKey(d => d.IdTipoCausaDevolucion)
                    .HasConstraintName("Relationship6");

                entity.HasOne(d => d.IdTipoDocumentacionNavigation)
                    .WithMany(p => p.AdslplusDocumentacion)
                    .HasForeignKey(d => d.IdTipoDocumentacion)
                    .HasConstraintName("Relationship2");

                entity.HasOne(d => d.IdTipoNovedadNavigation)
                    .WithMany(p => p.AdslplusDocumentacion)
                    .HasForeignKey(d => d.IdTipoNovedad)
                    .HasConstraintName("Relationship7");

                entity.HasOne(d => d.IdTransferenciaNavigation)
                    .WithMany(p => p.AdslplusDocumentacion)
                    .HasForeignKey(d => d.IdTransferencia)
                    .HasConstraintName("Relationship13");

                entity.HasOne(d => d.Aprovisionamiento)
                    .WithMany(p => p.AdslplusDocumentacion)
                    .HasForeignKey(d => new { d.AprovisionamientoLinea, d.AprovisionamientoSolicitud, d.AprovisionamientoFechaAgenda })
                    .HasConstraintName("fk_Aprovisionamiento");

                entity.HasOne(d => d.LineaBasica)
                    .WithMany(p => p.AdslplusDocumentacion)
                    .HasForeignKey(d => new { d.LineaBasicaSolicitud, d.LineaBasicaFecha, d.LineaBasicaDetalle })
                    .HasConstraintName("fk_LineaBasica");
            });

            modelBuilder.Entity<AdslplusDocumentacionHistorico>(entity =>
            {
                entity.HasKey(e => e.IdDocumentacionHistorico);

                entity.ToTable("ADSLPLUS_DOCUMENTACION_HISTORICO");

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.HasOne(d => d.IdDocumentacionNavigation)
                    .WithMany(p => p.AdslplusDocumentacionHistorico)
                    .HasForeignKey(d => d.IdDocumentacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Relationship3");

                entity.HasOne(d => d.IdEstadoDocumentacionNavigation)
                    .WithMany(p => p.AdslplusDocumentacionHistorico)
                    .HasForeignKey(d => d.IdEstadoDocumentacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Relationship4");

                entity.HasOne(d => d.IdTipoCausaDevolucionNavigation)
                    .WithMany(p => p.AdslplusDocumentacionHistorico)
                    .HasForeignKey(d => d.IdTipoCausaDevolucion)
                    .HasConstraintName("Relationship9");

                entity.HasOne(d => d.IdTipoNovedadNavigation)
                    .WithMany(p => p.AdslplusDocumentacionHistorico)
                    .HasForeignKey(d => d.IdTipoNovedad)
                    .HasConstraintName("Relationship8");

                entity.HasOne(d => d.IdTransferenciaNavigation)
                    .WithMany(p => p.AdslplusDocumentacionHistorico)
                    .HasForeignKey(d => d.IdTransferencia)
                    .HasConstraintName("Relationship14");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AdslplusDocumentacionHistorico)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_aspnet_Users");
            });

            modelBuilder.Entity<AdslplusEquipos>(entity =>
            {
                entity.HasKey(e => new { e.Serial, e.Codigo, e.Boleta });

                entity.ToTable("ADSLPLUS_EQUIPOS");

                entity.Property(e => e.Serial)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Codigo)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Boleta)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BodegaIn)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoEquipo)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.FechaIngreso).HasColumnType("datetime");

                entity.Property(e => e.FechaScaneo).HasColumnType("datetime");

                entity.HasOne(d => d.CodigoNavigation)
                    .WithMany(p => p.AdslplusEquipos)
                    .HasForeignKey(d => d.Codigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ADSLPLUS_EQUIPOS_ADSLPLUS_EQUIPOS_MODELOS");
            });

            modelBuilder.Entity<AdslplusEquiposKardex>(entity =>
            {
                entity.HasKey(e => new { e.FechaMovimiento, e.SerialEquipo, e.Bodega });

                entity.ToTable("ADSLPLUS_EQUIPOS_KARDEX");

                entity.Property(e => e.FechaMovimiento).HasColumnType("datetime");

                entity.Property(e => e.SerialEquipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Bodega)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Boleta)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaEntrada).HasColumnType("datetime");

                entity.Property(e => e.Linea)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.ObservacionesMov)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.SolicitudLogistica)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoMovimiento)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.TipoTrabajo)
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusEstadosDocumentacion>(entity =>
            {
                entity.HasKey(e => e.IdEstadoDocumentacion);

                entity.ToTable("ADSLPLUS_ESTADOS_DOCUMENTACION");

                entity.Property(e => e.IdEstadoDocumentacion).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<AdslplusFallasCpe>(entity =>
            {
                entity.HasKey(e => e.IdCodFalla);

                entity.ToTable("ADSLPLUS_FALLAS_CPE");

                entity.Property(e => e.IdCodFalla).ValueGeneratedNever();

                entity.Property(e => e.DescripcionFalla)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusGruposofertaLinb>(entity =>
            {
                entity.HasKey(e => e.Oferta);

                entity.ToTable("ADSLPLUS_GRUPOSOFERTA_LINB");

                entity.Property(e => e.Oferta)
                    .HasColumnName("OFERTA")
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Grupooferta)
                    .HasColumnName("GRUPOOFERTA")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusIdentificacionesExcluidas>(entity =>
            {
                entity.HasKey(e => e.Identificacion);

                entity.ToTable("ADSLPLUS_IDENTIFICACIONES_EXCLUIDAS");

                entity.Property(e => e.Identificacion)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<AdslplusLfaVillav>(entity =>
            {
                entity.HasKey(e => e.IdLfa);

                entity.ToTable("ADSLPLUS_LFA_VILLAV");

                entity.Property(e => e.IdLfa).HasColumnName("idLFA");

                entity.Property(e => e.CodCfa)
                    .IsRequired()
                    .HasColumnName("codCFA")
                    .HasMaxLength(50);

                entity.Property(e => e.CodRta)
                    .IsRequired()
                    .HasColumnName("codRTA")
                    .HasMaxLength(50);

                entity.Property(e => e.Lfa)
                    .IsRequired()
                    .HasColumnName("LFA")
                    .HasMaxLength(50);

                entity.Property(e => e.LfaDesc)
                    .IsRequired()
                    .HasColumnName("LFA_Desc")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<AdslplusLineabAgendas>(entity =>
            {
                entity.HasKey(e => new { e.SolicitudFk, e.FechaFk, e.DetalleFk, e.CtivoAgenda });

                entity.ToTable("ADSLPLUS_LINEAB_AGENDAS");

                entity.HasIndex(e => e.FechaFk)
                    .HasName("IndexNC_fechaFK");

                entity.Property(e => e.SolicitudFk)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFk).HasColumnType("datetime");

                entity.Property(e => e.DetalleFk)
                    .HasColumnName("DetalleFK")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoContrato)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFinRegistro).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraRegistro).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraRegistro2).HasColumnType("datetime");

                entity.Property(e => e.FechaUltimaAgenda).HasColumnType("datetime");

                entity.Property(e => e.IdTecnicoFk)
                    .IsRequired()
                    .HasColumnName("IdTecnicoFK")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.HasOne(d => d.AdslplusLineabasica)
                    .WithMany(p => p.AdslplusLineabAgendas)
                    .HasForeignKey(d => new { d.SolicitudFk, d.FechaFk, d.DetalleFk })
                    .HasConstraintName("FK_ADSLPLUS_LINEAB_AGENDAS_ADSLPLUS_LINEABASICA");
            });

            modelBuilder.Entity<AdslplusLineabAgendasBackUp>(entity =>
            {
                entity.HasKey(e => new { e.SolicitudFk, e.FechaFk, e.DetalleFk, e.CtivoAgenda });

                entity.ToTable("ADSLPLUS_LINEAB_AGENDAS_BackUp");

                entity.Property(e => e.SolicitudFk)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFk).HasColumnType("datetime");

                entity.Property(e => e.DetalleFk)
                    .HasColumnName("DetalleFK")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoContrato)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFinRegistro).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraRegistro).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraRegistro2).HasColumnType("datetime");

                entity.Property(e => e.FechaUltimaAgenda).HasColumnType("datetime");

                entity.Property(e => e.IdTecnicoFk)
                    .IsRequired()
                    .HasColumnName("IdTecnicoFK")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(2048)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusLineabasica>(entity =>
            {
                entity.HasKey(e => new { e.Solicitud, e.Fecha, e.Detalle });

                entity.ToTable("ADSLPLUS_LINEABASICA");

                entity.HasIndex(e => new { e.Fecha, e.CodCierre })
                    .HasName("NCI_CODCIERRE_LB");

                entity.HasIndex(e => new { e.Fecha, e.Solicitud, e.CodEstado })
                    .HasName("nci_wi_ADSLPLUS_LINEABASICA_4B77E043-35F7-445C-8FA2-619633BB5DBB");

                entity.HasIndex(e => new { e.Codigoa, e.Solicitud, e.Tipo, e.Identificacion, e.Detalle, e.Coordinacion, e.Fecha })
                    .HasName("ADSLPLUS_LINEABASICA_Indice1");

                entity.Property(e => e.Solicitud)
                    .HasColumnName("SOLICITUD")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Fecha)
                    .HasColumnName("FECHA")
                    .HasColumnType("datetime");

                entity.Property(e => e.Detalle)
                    .HasColumnName("DETALLE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Account)
                    .HasColumnName("ACCOUNT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Actividad)
                    .HasColumnName("ACTIVIDAD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Asesor)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Asignador)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AyudanteEtb)
                    .HasColumnName("AyudanteETB")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Barrio)
                    .HasColumnName("BARRIO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CanalVenta)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Central)
                    .HasColumnName("CENTRAL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Central1)
                    .HasColumnName("CENTRAL1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Clausula)
                    .HasColumnName("CLAUSULA")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Cliente)
                    .HasColumnName("CLIENTE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodCierre)
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.CodCierreAdsl)
                    .HasColumnName("CodCierreADSL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodEstado)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Codigoa)
                    .HasColumnName("CODIGOA")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Contacto)
                    .HasColumnName("CONTACTO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Contrato)
                    .HasColumnName("CONTRATO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Coordinacion)
                    .HasColumnName("COORDINACION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Depen)
                    .HasColumnName("DEPEN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DetVoip)
                    .HasColumnName("DET_VOIP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion)
                    .HasColumnName("DIRECCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion1)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion2)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion3)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DireccionCorrecta)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Distrito)
                    .HasColumnName("DISTRITO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Distritonew)
                    .HasColumnName("DISTRITOnew")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Documentacion)
                    .HasColumnName("DOCUMENTACION")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Entrega1)
                    .HasColumnName("ENTREGA1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Equipo)
                    .HasColumnName("EQUIPO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoContrato)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FecSol)
                    .HasColumnName("FEC_SOL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaCierre).HasColumnType("datetime");

                entity.Property(e => e.HoraAtencionAsesor)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HoraIni)
                    .HasColumnName("HORA_INI")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.IdArchivoGpbna).HasColumnName("IdArchivoGPBNA");

                entity.Property(e => e.IdArchivoRom).HasColumnName("IdArchivoROM");

                entity.Property(e => e.IdTecnico)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdentAuxTecnico)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Identificacion)
                    .HasColumnName("IDENTIFICACION")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ImagenGpnba)
                    .HasColumnName("imagenGPNBA")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Impresa)
                    .HasColumnName("IMPRESA")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.IpLan)
                    .HasColumnName("IP_LAN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IpLanRouter)
                    .HasColumnName("IP_LAN_ROUTER")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IpWanLocal)
                    .HasColumnName("IP_WAN_LOCAL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IpWanRemota)
                    .HasColumnName("IP_WAN_REMOTA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Liston)
                    .HasColumnName("LISTON")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MarcaEquipoRet)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MjDoc)
                    .HasColumnName("MJ_DOC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NDir)
                    .HasColumnName("N_DIR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NoPcs).HasColumnName("NO_PCS");

                entity.Property(e => e.ObservacionInst)
                    .HasColumnName("OBSERVACION_INST")
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.ParL)
                    .HasColumnName("PAR_L")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParS)
                    .HasColumnName("PAR_S")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("PASSWORD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pdatos)
                    .HasColumnName("PDATOS")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PersonaRecibe)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pin)
                    .HasColumnName("PIN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PlanDesmonte)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrimerUso)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Prioridad)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Pvoz)
                    .HasColumnName("PVoz")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReqRedLan)
                    .HasColumnName("REQ_RED_LAN")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ResultadoVisita)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Rvisita)
                    .HasColumnName("RVISITA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Secun)
                    .HasColumnName("SECUN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Serie)
                    .HasColumnName("SERIE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipoInst)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipoRet)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Slot)
                    .HasColumnName("SLOT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SolComercial)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SupercomboDesmonte)
                    .HasColumnName("SUPERCOMBO_DESMONTE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TDoc)
                    .HasColumnName("T_DOC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TelContacto)
                    .HasColumnName("TEL_CONTACTO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Terreno)
                    .HasColumnName("TERRENO")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Ticket)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo)
                    .HasColumnName("TIPO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoFalla)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TipoProducto)
                    .HasColumnName("TIPO_PRODUCTO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Usuario)
                    .HasColumnName("USUARIO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vci)
                    .HasColumnName("VCI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VciUsuario)
                    .HasColumnName("VCI_USUARIO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VpiUsuario)
                    .HasColumnName("VPI_USUARIO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.X)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusLineabasicaBackUp>(entity =>
            {
                entity.HasKey(e => new { e.Solicitud, e.Fecha, e.Detalle });

                entity.ToTable("ADSLPLUS_LINEABASICA_BackUp");

                entity.Property(e => e.Solicitud)
                    .HasColumnName("SOLICITUD")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Fecha)
                    .HasColumnName("FECHA")
                    .HasColumnType("datetime");

                entity.Property(e => e.Detalle)
                    .HasColumnName("DETALLE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Account)
                    .HasColumnName("ACCOUNT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Actividad)
                    .HasColumnName("ACTIVIDAD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Asesor)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Asignador)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AyudanteEtb)
                    .HasColumnName("AyudanteETB")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Barrio)
                    .HasColumnName("BARRIO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Central)
                    .HasColumnName("CENTRAL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Central1)
                    .HasColumnName("CENTRAL1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Clausula)
                    .HasColumnName("CLAUSULA")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Cliente)
                    .HasColumnName("CLIENTE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodCierre)
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.CodCierreAdsl)
                    .HasColumnName("CodCierreADSL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodEstado)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Codigoa)
                    .HasColumnName("CODIGOA")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Contacto)
                    .HasColumnName("CONTACTO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Coordinacion)
                    .HasColumnName("COORDINACION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Depen)
                    .HasColumnName("DEPEN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DetVoip)
                    .HasColumnName("DET_VOIP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion)
                    .HasColumnName("DIRECCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion1)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion2)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion3)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DireccionCorrecta)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Distrito)
                    .HasColumnName("DISTRITO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Distritonew)
                    .HasColumnName("DISTRITOnew")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Documentacion)
                    .HasColumnName("DOCUMENTACION")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Entrega1)
                    .HasColumnName("ENTREGA1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Equipo)
                    .HasColumnName("EQUIPO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoContrato)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FecSol)
                    .HasColumnName("FEC_SOL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaCierre).HasColumnType("datetime");

                entity.Property(e => e.HoraAtencionAsesor)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HoraIni)
                    .HasColumnName("HORA_INI")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.IdArchivoGpbna).HasColumnName("IdArchivoGPBNA");

                entity.Property(e => e.IdArchivoRom).HasColumnName("IdArchivoROM");

                entity.Property(e => e.IdTecnico)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdentAuxTecnico)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Identificacion)
                    .HasColumnName("IDENTIFICACION")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ImagenGpnba)
                    .HasColumnName("imagenGPNBA")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Impresa)
                    .HasColumnName("IMPRESA")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.IpLan)
                    .HasColumnName("IP_LAN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IpLanRouter)
                    .HasColumnName("IP_LAN_ROUTER")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IpWanLocal)
                    .HasColumnName("IP_WAN_LOCAL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IpWanRemota)
                    .HasColumnName("IP_WAN_REMOTA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Liston)
                    .HasColumnName("LISTON")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MarcaEquipoRet)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MjDoc)
                    .HasColumnName("MJ_DOC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NDir)
                    .HasColumnName("N_DIR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NoPcs).HasColumnName("NO_PCS");

                entity.Property(e => e.ObservacionInst)
                    .HasColumnName("OBSERVACION_INST")
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.ParL)
                    .HasColumnName("PAR_L")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParS)
                    .HasColumnName("PAR_S")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("PASSWORD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pdatos)
                    .HasColumnName("PDATOS")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PersonaRecibe)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pin)
                    .HasColumnName("PIN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PlanDesmonte)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrimerUso)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pvoz)
                    .HasColumnName("PVoz")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReqRedLan)
                    .HasColumnName("REQ_RED_LAN")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ResultadoVisita)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Rvisita)
                    .HasColumnName("RVISITA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Secun)
                    .HasColumnName("SECUN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Serie)
                    .HasColumnName("SERIE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipoInst)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipoRet)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Slot)
                    .HasColumnName("SLOT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SolComercial)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SupercomboDesmonte)
                    .HasColumnName("SUPERCOMBO_DESMONTE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TDoc)
                    .HasColumnName("T_DOC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TelContacto)
                    .HasColumnName("TEL_CONTACTO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Terreno)
                    .HasColumnName("TERRENO")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Ticket)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo)
                    .HasColumnName("TIPO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoFalla)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TipoProducto)
                    .HasColumnName("TIPO_PRODUCTO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Usuario)
                    .HasColumnName("USUARIO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vci)
                    .HasColumnName("VCI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VciUsuario)
                    .HasColumnName("VCI_USUARIO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VpiUsuario)
                    .HasColumnName("VPI_USUARIO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.X)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusLineabasicaHist>(entity =>
            {
                entity.HasKey(e => new { e.Solicitud, e.Fecha, e.FechaHoraReactiva });

                entity.ToTable("ADSLPLUS_LINEABASICA_HIST");

                entity.Property(e => e.Solicitud)
                    .HasColumnName("SOLICITUD")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Fecha)
                    .HasColumnName("FECHA")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaHoraReactiva).HasColumnType("datetime");

                entity.Property(e => e.Account)
                    .HasColumnName("ACCOUNT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Actividad)
                    .HasColumnName("ACTIVIDAD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Asesor)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Asignador)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AyudanteEtb)
                    .HasColumnName("AyudanteETB")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Barrio)
                    .HasColumnName("BARRIO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Central)
                    .HasColumnName("CENTRAL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Central1)
                    .HasColumnName("CENTRAL1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Clausula)
                    .HasColumnName("CLAUSULA")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Cliente)
                    .HasColumnName("CLIENTE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodCierre)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CodCierreAdsl)
                    .HasColumnName("CodCierreADSL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodEstado)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Codigoa)
                    .HasColumnName("CODIGOA")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Contacto)
                    .HasColumnName("CONTACTO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Coordinacion)
                    .HasColumnName("COORDINACION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Depen)
                    .HasColumnName("DEPEN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DetVoip)
                    .HasColumnName("DET_VOIP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Detalle)
                    .HasColumnName("DETALLE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion)
                    .HasColumnName("DIRECCION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion1)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion2)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion3)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Distrito)
                    .HasColumnName("DISTRITO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Distritonew)
                    .HasColumnName("DISTRITOnew")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Documentacion)
                    .HasColumnName("DOCUMENTACION")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Entrega1)
                    .HasColumnName("ENTREGA1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Equipo)
                    .HasColumnName("EQUIPO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoContrato)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FecSol)
                    .HasColumnName("FEC_SOL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaCierre).HasColumnType("datetime");

                entity.Property(e => e.HoraAtencionAsesor)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HoraIni)
                    .HasColumnName("HORA_INI")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.IdTecnico)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Identificacion)
                    .HasColumnName("IDENTIFICACION")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ImagenGpnba)
                    .HasColumnName("imagenGPNBA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Impresa)
                    .HasColumnName("IMPRESA")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.IpLan)
                    .HasColumnName("IP_LAN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IpLanRouter)
                    .HasColumnName("IP_LAN_ROUTER")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IpWanLocal)
                    .HasColumnName("IP_WAN_LOCAL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IpWanRemota)
                    .HasColumnName("IP_WAN_REMOTA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Liston)
                    .HasColumnName("LISTON")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MarcaEquipoRet)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MjDoc)
                    .HasColumnName("MJ_DOC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NDir)
                    .HasColumnName("N_DIR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NoPcs).HasColumnName("NO_PCS");

                entity.Property(e => e.ObservacionInst)
                    .HasColumnName("OBSERVACION_INST")
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.ParL)
                    .HasColumnName("PAR_L")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParS)
                    .HasColumnName("PAR_S")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("PASSWORD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pdatos)
                    .HasColumnName("PDATOS")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PersonaRecibe)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pin)
                    .HasColumnName("PIN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PlanDesmonte)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrimerUso)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pvoz)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReqRedLan)
                    .HasColumnName("REQ_RED_LAN")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ResultadoVisita)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Rvisita)
                    .HasColumnName("RVISITA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Secun)
                    .HasColumnName("SECUN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Serie)
                    .HasColumnName("SERIE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipoInst)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerieEquipoRet)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Slot)
                    .HasColumnName("SLOT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SolComercial)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SupercomboDesmonte)
                    .HasColumnName("SUPERCOMBO_DESMONTE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TDoc)
                    .HasColumnName("T_DOC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TelContacto)
                    .HasColumnName("TEL_CONTACTO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Terreno)
                    .HasColumnName("TERRENO")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Ticket)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo)
                    .HasColumnName("TIPO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoFalla)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TipoProducto)
                    .HasColumnName("TIPO_PRODUCTO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Usuario)
                    .HasColumnName("USUARIO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vci)
                    .HasColumnName("VCI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VciUsuario)
                    .HasColumnName("VCI_USUARIO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VpiUsuario)
                    .HasColumnName("VPI_USUARIO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.X)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusListaMaterial>(entity =>
            {
                entity.HasKey(e => e.CodTipMaterial);

                entity.ToTable("ADSLPLUS_LISTA_MATERIAL");

                entity.Property(e => e.CodTipMaterial).ValueGeneratedNever();

                entity.Property(e => e.DesTipMateriall)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UnidadMedida).HasMaxLength(7);
            });

            modelBuilder.Entity<AdslplusLogActualizacionEquipos>(entity =>
            {
                entity.HasKey(e => new { e.FechaHoraRegistro, e.SerialOriginal });

                entity.ToTable("ADSLPLUS_LogActualizacionEquipos");

                entity.Property(e => e.FechaHoraRegistro).HasColumnType("datetime");

                entity.Property(e => e.SerialOriginal)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fecha)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Linea)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SerialNuevo)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusLogParameventos>(entity =>
            {
                entity.HasKey(e => e.IdlogParametro);

                entity.ToTable("ADSLPLUS_LOG_PARAMEVENTOS");

                entity.Property(e => e.DatFechaActualizacion)
                    .HasColumnName("datFechaActualizacion")
                    .HasColumnType("datetime");

                entity.Property(e => e.DatFechaRegistro)
                    .HasColumnName("datFechaRegistro")
                    .HasColumnType("datetime");

                entity.Property(e => e.FloValMayEnFranja).HasColumnName("floValMayEnFranja");

                entity.Property(e => e.FloValMayFueraFranja).HasColumnName("floValMayFueraFranja");

                entity.Property(e => e.FloValMenEnfranja).HasColumnName("floValMenEnfranja");

                entity.Property(e => e.FloValMenFueraFranja).HasColumnName("floValMenFueraFranja");

                entity.Property(e => e.IntCantminEnFranja).HasColumnName("intCantminEnFranja");

                entity.Property(e => e.IntCantminFueraFranja).HasColumnName("intCantminFueraFranja");

                entity.Property(e => e.VarProducto)
                    .HasColumnName("varProducto")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VarUsuarioActualiza)
                    .HasColumnName("varUsuarioActualiza")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VarUsuarioRegistra)
                    .HasColumnName("varUsuarioRegistra")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusLogSolicitudes>(entity =>
            {
                entity.HasKey(e => new { e.SolicitudId, e.FechaSolicitud });

                entity.ToTable("ADSLPLUS_LOG_SOLICITUDES");

                entity.Property(e => e.SolicitudId)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FechaSolicitud).HasColumnType("datetime");
            });

            modelBuilder.Entity<AdslplusLogUsuarios>(entity =>
            {
                entity.HasKey(e => new { e.UserName, e.FechaRegistro });

                entity.ToTable("ADSLPLUS_LOG_USUARIOS");

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaRegistro).HasColumnType("datetime");

                entity.Property(e => e.Evento)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Justificacion)
                    .HasMaxLength(512)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusMarcasEquipos>(entity =>
            {
                entity.HasKey(e => new { e.SerieId, e.Marca, e.Modelo });

                entity.ToTable("ADSLPLUS_MARCAS_EQUIPOS");

                entity.Property(e => e.SerieId)
                    .HasColumnName("SerieID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Marca)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Modelo)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusMaterialesKardex>(entity =>
            {
                entity.HasKey(e => new { e.FechaMovimiento, e.CodigoMaterial, e.Bodega });

                entity.ToTable("ADSLPLUS_MATERIALES_KARDEX");

                entity.Property(e => e.FechaMovimiento).HasColumnType("datetime");

                entity.Property(e => e.CodigoMaterial)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Bodega)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BoletaIngreso)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CantidadMaterial).HasColumnType("numeric(8, 2)");

                entity.Property(e => e.EstadoMaterial).HasDefaultValueSql("((1))");

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.Linea)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.ObservacionesMov)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.SolicitudLogistica)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoMovimiento)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.TipoTrabajo).HasMaxLength(3);
            });

            modelBuilder.Entity<AdslplusMaximosTecnico>(entity =>
            {
                entity.HasKey(e => e.NombreElemento);

                entity.ToTable("ADSLPLUS_MAXIMOS_TECNICO");

                entity.Property(e => e.NombreElemento)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<AdslplusNovedad>(entity =>
            {
                entity.HasKey(e => e.IdNovedad);

                entity.ToTable("ADSLPLUS_NOVEDAD");

                entity.Property(e => e.FechaSolicitud).HasColumnType("date");

                entity.Property(e => e.Fecharegistro).HasColumnType("date");

                entity.Property(e => e.Linea)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Novedad)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Solicitud)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.UsuarioRegistro)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusParameventos>(entity =>
            {
                entity.HasKey(e => e.IdParametro);

                entity.ToTable("ADSLPLUS_PARAMEVENTOS");

                entity.Property(e => e.DatFechaRegistro)
                    .HasColumnName("datFechaRegistro")
                    .HasColumnType("datetime");

                entity.Property(e => e.FloValMayEnFranja).HasColumnName("floValMayEnFranja");

                entity.Property(e => e.FloValMayFueraFranja).HasColumnName("floValMayFueraFranja");

                entity.Property(e => e.FloValMenEnfranja).HasColumnName("floValMenEnfranja");

                entity.Property(e => e.FloValMenFueraFranja).HasColumnName("floValMenFueraFranja");

                entity.Property(e => e.IntCantminEnFranja).HasColumnName("intCantminEnFranja");

                entity.Property(e => e.IntCantminFueraFranja).HasColumnName("intCantminFueraFranja");

                entity.Property(e => e.VarProducto)
                    .HasColumnName("varProducto")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VarUsuarioRegistra)
                    .HasColumnName("varUsuarioRegistra")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusPqrs>(entity =>
            {
                entity.HasKey(e => new { e.Linea, e.FechaRegistro });

                entity.ToTable("ADSLPLUS_PQRS");

                entity.Property(e => e.Linea)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.FechaRegistro).HasColumnType("datetime");

                entity.Property(e => e.Aplica).HasMaxLength(2);

                entity.Property(e => e.Asesor)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.CedulaCliente)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Central)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DescripcionDp)
                    .HasColumnName("DescripcionDP")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Distrito)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Estado)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.FechaAgenda).HasColumnType("datetime");

                entity.Property(e => e.FechaCierre).HasColumnType("datetime");

                entity.Property(e => e.Liston)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LoginTecnico)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.NombreCliente)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NumDp)
                    .HasColumnName("NumDP")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.NumSolicitud)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Oferta)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParL)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParS)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PosDatos)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pvoz)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Respuesta)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.Secundaria)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ticket)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.TipologiaDp).HasColumnName("TipologiaDP");
            });

            modelBuilder.Entity<AdslplusProdEquipos>(entity =>
            {
                entity.HasKey(e => e.Serial);

                entity.ToTable("ADSLPLUS_PROD_EQUIPOS");

                entity.Property(e => e.Serial)
                    .HasMaxLength(25)
                    .ValueGeneratedNever();

                entity.Property(e => e.IdciudInvOrg)
                    .HasColumnName("IDCiudInvOrg")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.IdciudadInv)
                    .HasColumnName("IDCiudadInv")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Imei)
                    .HasColumnName("IMEI")
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<AdslplusProdEquiposHis>(entity =>
            {
                entity.HasKey(e => new { e.Serial, e.FechaReingreso });

                entity.ToTable("ADSLPLUS_PROD_EQUIPOS_HIS");

                entity.Property(e => e.Serial).HasMaxLength(25);

                entity.Property(e => e.FechaReingreso).HasColumnType("datetime");

                entity.Property(e => e.IdciudInvOrg)
                    .HasColumnName("IDCiudInvOrg")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.IdciudadInv)
                    .HasColumnName("IDCiudadInv")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Imei)
                    .HasColumnName("IMEI")
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<AdslplusProdMaterial>(entity =>
            {
                entity.HasKey(e => new { e.Boleta, e.CodTipMaterial });

                entity.ToTable("ADSLPLUS_PROD_MATERIAL");

                entity.Property(e => e.Boleta).HasMaxLength(30);

                entity.Property(e => e.UnidadMedida).HasMaxLength(10);
            });

            modelBuilder.Entity<AdslplusProductoCodigo>(entity =>
            {
                entity.HasKey(e => e.CodTipProd);

                entity.ToTable("ADSLPLUS_Producto_Codigo");

                entity.Property(e => e.CodTipProd)
                    .HasMaxLength(10)
                    .ValueGeneratedNever();

                entity.Property(e => e.DesTipProd)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<AdslplusRegistroMovEquipo>(entity =>
            {
                entity.HasKey(e => new { e.Boleta, e.IdEquipo, e.FechaRegistro });

                entity.ToTable("ADSLPLUS_REGISTRO_MOV_EQUIPO");

                entity.HasIndex(e => e.IdEquipo)
                    .HasName("CI_Id_equipo");

                entity.HasIndex(e => new { e.IdTecnicoFinal, e.CodTipMov })
                    .HasName("nci_wi_ADSLPLUS_REGISTRO_MOV_EQUIPO_1198F6BDFD83356EAFA62E7E8756C2CF");

                entity.HasIndex(e => new { e.Boleta, e.FechaRegistro, e.IdEquipo, e.IdTecnicoInicial, e.CodTipMov })
                    .HasName("nci_wi_ADSLPLUS_REGISTRO_MOV_EQUIPO_9401C846-BC47-4E29-BA87-B1A1D8FC5325");

                entity.HasIndex(e => new { e.Boleta, e.IdEquipo, e.FechaRegistro, e.IdTecnicoInicial, e.CodTipMov })
                    .HasName("nci_wi_ADSLPLUS_REGISTRO_MOV_EQUIPO_76565112D1AD223D3BDD");

                entity.Property(e => e.Boleta).HasMaxLength(25);

                entity.Property(e => e.IdEquipo)
                    .HasColumnName("Id_Equipo")
                    .HasMaxLength(25);

                entity.Property(e => e.FechaRegistro).HasColumnType("datetime");

                entity.Property(e => e.CcSolicitante).HasColumnName("CC_Solicitante");

                entity.Property(e => e.CodColvatel).HasMaxLength(20);

                entity.Property(e => e.FechaVencimiento).HasColumnType("datetime");

                entity.Property(e => e.IdBodCiudadFinal)
                    .IsRequired()
                    .HasColumnName("Id_BodCiudad_Final")
                    .HasMaxLength(15);

                entity.Property(e => e.IdBodCiudadInicial)
                    .IsRequired()
                    .HasColumnName("Id_BodCiudad_Inicial")
                    .HasMaxLength(15);

                entity.Property(e => e.IdBodZonaFinal)
                    .HasColumnName("Id_BodZona_Final")
                    .HasMaxLength(15);

                entity.Property(e => e.IdBodZonaInicial)
                    .HasColumnName("Id_BodZona_Inicial")
                    .HasMaxLength(15);

                entity.Property(e => e.IdTecnicoFinal).HasColumnName("Id_Tecnico_Final");

                entity.Property(e => e.IdTecnicoInicial).HasColumnName("Id_Tecnico_Inicial");

                entity.Property(e => e.NFomrImf04)
                    .HasColumnName("N_Fomr_IMF04")
                    .HasMaxLength(20);

                entity.Property(e => e.ValeAcompañamiento).HasMaxLength(256);
            });

            modelBuilder.Entity<AdslplusRegistroMovMaterial>(entity =>
            {
                entity.HasKey(e => new { e.NSolicitud, e.Boleta, e.CodTipMaterial, e.EnDerivacion });

                entity.ToTable("ADSLPLUS_REGISTRO_MOV_MATERIAL");

                entity.HasIndex(e => new { e.CodTipMov, e.CodTipoUso, e.FechaRegistro })
                    .HasName("nci_wi_ADSLPLUS_REGISTRO_MOV_MATERIAL_A01A0107D4A99B000BA7D25C5146E450");

                entity.HasIndex(e => new { e.Cantidad, e.CodTipMaterial, e.CodTipoUso, e.IdTecnicoFin, e.CodTipMov })
                    .HasName("nci_wi_ADSLPLUS_REGISTRO_MOV_MATERIAL_F5D08D8A-918E-499A-A5F8-4AED8B82C794");

                entity.HasIndex(e => new { e.Cantidad, e.CodTipMaterial, e.CodTipoUso, e.IdTecnicoIni, e.CodTipMov })
                    .HasName("nci_wi_ADSLPLUS_REGISTRO_MOV_MATERIAL_DBE7B6899E35C6A9BDDF");

                entity.HasIndex(e => new { e.Boleta, e.Cantidad, e.CodTipMaterial, e.CodTipoUso, e.IdBodCiudadIni, e.CodTipMov })
                    .HasName("nci_wi_ADSLPLUS_REGISTRO_MOV_MATERIAL_B7D9F1592888FACDAA20");

                entity.Property(e => e.NSolicitud)
                    .HasColumnName("N_Solicitud")
                    .HasMaxLength(35);

                entity.Property(e => e.Boleta).HasMaxLength(35);

                entity.Property(e => e.CcSolicitante).HasColumnName("CC_Solicitante");

                entity.Property(e => e.CodColvatel).HasMaxLength(20);

                entity.Property(e => e.FechaRegistro).HasColumnType("datetime");

                entity.Property(e => e.FechaVencimiento).HasColumnType("datetime");

                entity.Property(e => e.IdBodCiudadFin)
                    .IsRequired()
                    .HasColumnName("Id_BodCiudad_Fin")
                    .HasMaxLength(15);

                entity.Property(e => e.IdBodCiudadIni)
                    .IsRequired()
                    .HasColumnName("Id_BodCiudad_Ini")
                    .HasMaxLength(15);

                entity.Property(e => e.IdBodZonaFin)
                    .HasColumnName("Id_BodZona_Fin")
                    .HasMaxLength(15);

                entity.Property(e => e.IdBodZonaIni)
                    .HasColumnName("Id_BodZona_Ini")
                    .HasMaxLength(15);

                entity.Property(e => e.IdTecnicoFin).HasColumnName("Id_Tecnico_Fin");

                entity.Property(e => e.IdTecnicoIni).HasColumnName("Id_Tecnico_Ini");

                entity.Property(e => e.NFormImf04)
                    .HasColumnName("N_Form_IMF04")
                    .HasMaxLength(20);

                entity.Property(e => e.UnidadMedida).HasMaxLength(10);

                entity.Property(e => e.ValeAcompañamiento).HasMaxLength(20);
            });

            modelBuilder.Entity<AdslplusRtaVillav>(entity =>
            {
                entity.HasKey(e => e.Idrta);

                entity.ToTable("ADSLPLUS_RTA_VILLAV");

                entity.Property(e => e.Idrta).HasColumnName("IDRTA");

                entity.Property(e => e.Rta)
                    .IsRequired()
                    .HasColumnName("RTA")
                    .HasMaxLength(50);

                entity.Property(e => e.RtaDesc)
                    .IsRequired()
                    .HasColumnName("RTA_Desc")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<AdslplusSegmentos>(entity =>
            {
                entity.HasKey(e => e.IntIdOferta);

                entity.ToTable("ADSLPLUS_SEGMENTOS");

                entity.Property(e => e.IntIdOferta).HasColumnName("intIdOferta");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.DatFechaRegistro)
                    .HasColumnName("datFechaRegistro")
                    .HasColumnType("datetime");

                entity.Property(e => e.VarOferta)
                    .HasColumnName("varOferta")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VarProducto)
                    .HasColumnName("varProducto")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VarTipo)
                    .HasColumnName("varTipo")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusSolicitudes>(entity =>
            {
                entity.HasKey(e => e.SolicitudId);

                entity.ToTable("ADSLPLUS_SOLICITUDES");

                entity.HasIndex(e => new { e.Observaciones, e.Estado, e.FechaSolicitud })
                    .HasName("nci_wi_ADSLPLUS_SOLICITUDES_6E1DB22CA9E715A0E26CC26058CF769B");

                entity.HasIndex(e => new { e.CodTipoUso, e.FechaSolicitud, e.SolicitudId, e.UsuarioUid, e.ZonaFk, e.CedulaEmpleado, e.Estado, e.TipoSolicitud })
                    .HasName("nci_wi_ADSLPLUS_SOLICITUDES_E625A01D-4A30-4FDA-8A8F-F70F9CBA560C");

                entity.Property(e => e.SolicitudId)
                    .HasMaxLength(30)
                    .ValueGeneratedNever();

                entity.Property(e => e.CedulaEmpleado)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.CentroCostosFk)
                    .IsRequired()
                    .HasColumnName("CentroCostosFK")
                    .HasMaxLength(7);

                entity.Property(e => e.Estado).HasMaxLength(3);

                entity.Property(e => e.FechaEstado).HasColumnType("datetime");

                entity.Property(e => e.FechaSolicitud).HasColumnType("datetime");

                entity.Property(e => e.MotivoAnulacion).HasMaxLength(50);

                entity.Property(e => e.Observaciones).HasMaxLength(255);

                entity.Property(e => e.TipoSolicitud)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Uidestado).HasColumnName("UIDEstado");

                entity.Property(e => e.UsuarioUid).HasColumnName("UsuarioUID");

                entity.Property(e => e.ZonaFk)
                    .HasColumnName("ZonaFK")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<AdslplusSolicitudesDetalle>(entity =>
            {
                entity.HasKey(e => new { e.SolicitudIdFk, e.IdCodigoFk, e.Item });

                entity.ToTable("ADSLPLUS_SOLICITUDES_DETALLE");

                entity.Property(e => e.SolicitudIdFk)
                    .HasColumnName("SolicitudIdFK")
                    .HasMaxLength(30);

                entity.Property(e => e.IdCodigoFk)
                    .HasColumnName("IdCodigoFK")
                    .HasMaxLength(15);

                entity.Property(e => e.CantEntregada).HasMaxLength(25);

                entity.Property(e => e.LineaUso)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.Serial).HasMaxLength(30);
            });

            modelBuilder.Entity<AdslplusSolicitudHistoria>(entity =>
            {
                entity.HasKey(e => new { e.IdSolicitud, e.Estado });

                entity.ToTable("ADSLPLUS_SOLICITUD_HISTORIA");

                entity.Property(e => e.IdSolicitud).HasMaxLength(30);

                entity.Property(e => e.Estado).HasMaxLength(3);

                entity.Property(e => e.FechaEstado).HasColumnType("datetime");

                entity.Property(e => e.UsrId).HasColumnName("UsrID");
            });

            modelBuilder.Entity<AdslplusTecnicos>(entity =>
            {
                entity.HasKey(e => e.IdEmpleadoFk);

                entity.ToTable("ADSLPLUS_TECNICOS");

                entity.HasIndex(e => e.UserIdFk)
                    .HasName("IdTecnico_indice");

                entity.Property(e => e.IdEmpleadoFk)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CeluarPersonal)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.CelularCorporativo)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoSecreto)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NoEtb)
                    .HasColumnName("NoETB")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.TelefonoFijo)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TipoTecnico)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.UserIdFk).HasColumnName("UserIdFK");

                entity.Property(e => e.ZonaFk).HasColumnName("ZonaFK");
            });

            modelBuilder.Entity<AdslplusTipoMovimientos>(entity =>
            {
                entity.HasKey(e => e.CodTipMov);

                entity.ToTable("ADSLPLUS_TIPO_MOVIMIENTOS");

                entity.Property(e => e.CodTipMov).ValueGeneratedNever();

                entity.Property(e => e.DesTipMov)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<AdslplusTiposAgendas>(entity =>
            {
                entity.HasKey(e => e.IdTipoAgenda);

                entity.ToTable("ADSLPLUS_TIPOS_AGENDAS");

                entity.Property(e => e.IdTipoAgenda).ValueGeneratedNever();

                entity.Property(e => e.TpoAgenda).HasMaxLength(50);
            });

            modelBuilder.Entity<AdslplusTiposCausaDevolucion>(entity =>
            {
                entity.HasKey(e => e.IdTipoCausaDevolucion);

                entity.ToTable("ADSLPLUS_TIPOS_CAUSA_DEVOLUCION");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<AdslplusTiposDocumentacion>(entity =>
            {
                entity.HasKey(e => e.IdTipoDocumentacion);

                entity.ToTable("ADSLPLUS_TIPOS_DOCUMENTACION");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<AdslplusTiposNovedad>(entity =>
            {
                entity.HasKey(e => e.IdTipoNovedad);

                entity.ToTable("ADSLPLUS_TIPOS_NOVEDAD");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<AdslplusTiposOferta>(entity =>
            {
                entity.HasKey(e => new { e.TipoServicio, e.TipoOferta });

                entity.ToTable("ADSLPLUS_TIPOS_OFERTA");

                entity.Property(e => e.TipoServicio).HasMaxLength(30);

                entity.Property(e => e.TipoOferta)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdTipoDocumentacionNavigation)
                    .WithMany(p => p.AdslplusTiposOferta)
                    .HasForeignKey(d => d.IdTipoDocumentacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Relationship5");
            });

            modelBuilder.Entity<AdslplusTiposPqrs>(entity =>
            {
                entity.HasKey(e => e.IdTipologia);

                entity.ToTable("ADSLPLUS_TIPOS_PQRS");

                entity.Property(e => e.IdTipologia).ValueGeneratedNever();

                entity.Property(e => e.Tipologia)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusTiposPrioridadAgendas>(entity =>
            {
                entity.HasKey(e => e.IdTipoPrioridadAgenda);

                entity.ToTable("ADSLPLUS_TIPOS_PRIORIDAD_AGENDAS");

                entity.Property(e => e.IdTipoPrioridadAgenda).ValueGeneratedNever();

                entity.Property(e => e.TpoPrioridad).HasMaxLength(50);

                entity.Property(e => e.Tpsigla)
                    .HasColumnName("TPSigla")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<AdslplusTransferencia>(entity =>
            {
                entity.HasKey(e => e.IdTransferencia);

                entity.ToTable("ADSLPLUS_TRANSFERENCIA");

                entity.Property(e => e.Fecha).HasColumnType("datetime");
            });

            modelBuilder.Entity<AdslplusTransferenciaDetalle>(entity =>
            {
                entity.HasKey(e => new { e.IdTransferencia, e.IdDocumentacion });

                entity.ToTable("ADSLPLUS_TRANSFERENCIA_DETALLE");

                entity.HasOne(d => d.IdDocumentacionNavigation)
                    .WithMany(p => p.AdslplusTransferenciaDetalle)
                    .HasForeignKey(d => d.IdDocumentacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Relationship11");

                entity.HasOne(d => d.IdTransferenciaNavigation)
                    .WithMany(p => p.AdslplusTransferenciaDetalle)
                    .HasForeignKey(d => d.IdTransferencia)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Relationship10");
            });

            modelBuilder.Entity<AdslplusTransportes>(entity =>
            {
                entity.HasKey(e => new { e.LineaFk, e.SolicitudFk, e.FechaAgendaFk, e.CtivoSolicitud });

                entity.ToTable("ADSLPLUS_TRANSPORTES");

                entity.Property(e => e.LineaFk)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.SolicitudFk)
                    .HasColumnName("SolicitudFK")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.FechaAgendaFk)
                    .HasColumnName("FechaAgendaFK")
                    .HasColumnType("datetime");

                entity.Property(e => e.CentralDestino)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DireccionDestino)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Estado)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.FechaDestino).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraAtencion).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraRegistro).HasColumnType("datetime");

                entity.Property(e => e.IdTecnicoFk)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.LineaDestino)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.ObservacionesAsig)
                    .HasMaxLength(1048)
                    .IsUnicode(false);

                entity.Property(e => e.PlacaAsignada)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.SolicitudDestino)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Ticket)
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdslplusUsuarios>(entity =>
            {
                entity.HasKey(e => e.IdEmpleadoFk);

                entity.ToTable("ADSLPLUS_USUARIOS");

                entity.HasIndex(e => e.UserIdFk)
                    .HasName("Idusuario_indice");

                entity.Property(e => e.IdEmpleadoFk)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.UserIdFk).HasColumnName("UserIdFK");

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ZonaFk).HasColumnName("ZonaFK");
            });

            modelBuilder.Entity<AdslplusUsuariosCoordinaciones>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.Coordinacion });

                entity.ToTable("ADSLPLUS_USUARIOS_COORDINACIONES");

                entity.Property(e => e.Coordinacion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AdslplusUsuariosCoordinaciones)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_UsuCoor_aspnet_Users");
            });

            modelBuilder.Entity<AdslplusZonas>(entity =>
            {
                entity.HasKey(e => e.IdZona);

                entity.ToTable("ADSLPLUS_ZONAS");

                entity.Property(e => e.IdZona).ValueGeneratedNever();

                entity.Property(e => e.IdBodega)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.NombreZona)
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AspnetApplications>(entity =>
            {
                entity.HasKey(e => e.ApplicationId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("aspnet_Applications");

                entity.HasIndex(e => e.ApplicationName)
                    .HasName("UQ__aspnet_A__3091033179F28717")
                    .IsUnique();

                entity.HasIndex(e => e.LoweredApplicationName)
                    .HasName("aspnet_Applications_Index")
                    .ForSqlServerIsClustered();

                entity.Property(e => e.ApplicationId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ApplicationName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.Description).HasMaxLength(256);

                entity.Property(e => e.LoweredApplicationName)
                    .IsRequired()
                    .HasMaxLength(256);
            });

            modelBuilder.Entity<AspnetMembership>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("aspnet_Membership");

                entity.HasIndex(e => new { e.ApplicationId, e.LoweredEmail })
                    .HasName("aspnet_Membership_index")
                    .ForSqlServerIsClustered();

                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.Comment).HasColumnType("ntext");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.FailedPasswordAnswerAttemptWindowStart).HasColumnType("datetime");

                entity.Property(e => e.FailedPasswordAttemptWindowStart).HasColumnType("datetime");

                entity.Property(e => e.LastLockoutDate).HasColumnType("datetime");

                entity.Property(e => e.LastLoginDate).HasColumnType("datetime");

                entity.Property(e => e.LastPasswordChangedDate).HasColumnType("datetime");

                entity.Property(e => e.LoweredEmail).HasMaxLength(256);

                entity.Property(e => e.MobilePin)
                    .HasColumnName("MobilePIN")
                    .HasMaxLength(16);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.PasswordAnswer).HasMaxLength(128);

                entity.Property(e => e.PasswordQuestion).HasMaxLength(256);

                entity.Property(e => e.PasswordSalt)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.AspnetMembership)
                    .HasForeignKey(d => d.ApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Me__Appli__2EFAF1E2");

                entity.HasOne(d => d.User)
                    .WithOne(p => p.AspnetMembership)
                    .HasForeignKey<AspnetMembership>(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Me__UserI__2FEF161B");
            });

            modelBuilder.Entity<AspnetPaths>(entity =>
            {
                entity.HasKey(e => e.PathId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("aspnet_Paths");

                entity.HasIndex(e => new { e.ApplicationId, e.LoweredPath })
                    .HasName("aspnet_Paths_index")
                    .IsUnique()
                    .ForSqlServerIsClustered();

                entity.Property(e => e.PathId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.LoweredPath)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.AspnetPaths)
                    .HasForeignKey(d => d.ApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Pa__Appli__31D75E8D");
            });

            modelBuilder.Entity<AspnetPersonalizationAllUsers>(entity =>
            {
                entity.HasKey(e => e.PathId);

                entity.ToTable("aspnet_PersonalizationAllUsers");

                entity.Property(e => e.PathId).ValueGeneratedNever();

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.PageSettings)
                    .IsRequired()
                    .HasColumnType("image");

                entity.HasOne(d => d.Path)
                    .WithOne(p => p.AspnetPersonalizationAllUsers)
                    .HasForeignKey<AspnetPersonalizationAllUsers>(d => d.PathId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Pe__PathI__33BFA6FF");
            });

            modelBuilder.Entity<AspnetPersonalizationPerUser>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("aspnet_PersonalizationPerUser");

                entity.HasIndex(e => new { e.PathId, e.UserId })
                    .HasName("aspnet_PersonalizationPerUser_index1")
                    .IsUnique()
                    .ForSqlServerIsClustered();

                entity.HasIndex(e => new { e.UserId, e.PathId })
                    .HasName("aspnet_PersonalizationPerUser_ncindex2")
                    .IsUnique();

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.PageSettings)
                    .IsRequired()
                    .HasColumnType("image");

                entity.HasOne(d => d.Path)
                    .WithMany(p => p.AspnetPersonalizationPerUser)
                    .HasForeignKey(d => d.PathId)
                    .HasConstraintName("FK__aspnet_Pe__PathI__369C13AA");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspnetPersonalizationPerUser)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__aspnet_Pe__UserI__379037E3");
            });

            modelBuilder.Entity<AspnetProfile>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("aspnet_Profile");

                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.PropertyNames)
                    .IsRequired()
                    .HasColumnType("ntext");

                entity.Property(e => e.PropertyValuesBinary)
                    .IsRequired()
                    .HasColumnType("image");

                entity.Property(e => e.PropertyValuesString)
                    .IsRequired()
                    .HasColumnType("ntext");

                entity.HasOne(d => d.User)
                    .WithOne(p => p.AspnetProfile)
                    .HasForeignKey<AspnetProfile>(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Pr__UserI__39788055");
            });

            modelBuilder.Entity<AspnetRoles>(entity =>
            {
                entity.HasKey(e => e.RoleId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("aspnet_Roles");

                entity.HasIndex(e => new { e.ApplicationId, e.LoweredRoleName })
                    .HasName("aspnet_Roles_index1")
                    .IsUnique()
                    .ForSqlServerIsClustered();

                entity.Property(e => e.RoleId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Description).HasMaxLength(256);

                entity.Property(e => e.LoweredRoleName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.AspnetRoles)
                    .HasForeignKey(d => d.ApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Ro__Appli__3B60C8C7");
            });

            modelBuilder.Entity<AspnetSchemaVersions>(entity =>
            {
                entity.HasKey(e => new { e.Feature, e.CompatibleSchemaVersion });

                entity.ToTable("aspnet_SchemaVersions");

                entity.Property(e => e.Feature).HasMaxLength(128);

                entity.Property(e => e.CompatibleSchemaVersion).HasMaxLength(128);
            });

            modelBuilder.Entity<AspnetUsers>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("aspnet_Users");

                entity.HasIndex(e => e.UserName)
                    .HasName("aspnet_Users_Indice1");

                entity.HasIndex(e => new { e.ApplicationId, e.LastActivityDate })
                    .HasName("aspnet_Users_Index2");

                entity.HasIndex(e => new { e.ApplicationId, e.LoweredUserName })
                    .HasName("aspnet_Users_Index")
                    .IsUnique()
                    .ForSqlServerIsClustered();

                entity.Property(e => e.UserId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.LastActivityDate).HasColumnType("datetime");

                entity.Property(e => e.LoweredUserName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.MobileAlias).HasMaxLength(16);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.AspnetUsers)
                    .HasForeignKey(d => d.ApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Us__Appli__3E3D3572");
            });

            modelBuilder.Entity<AspnetUsersInRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.ToTable("aspnet_UsersInRoles");

                entity.HasIndex(e => e.RoleId)
                    .HasName("aspnet_UsersInRoles_index");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspnetUsersInRoles)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Us__RoleI__43F60EC8");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspnetUsersInRoles)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__aspnet_Us__UserI__420DC656");
            });

            modelBuilder.Entity<AspnetWebEventEvents>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.ToTable("aspnet_WebEvent_Events");

                entity.Property(e => e.EventId)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ApplicationPath).HasMaxLength(256);

                entity.Property(e => e.ApplicationVirtualPath).HasMaxLength(256);

                entity.Property(e => e.Details).HasColumnType("ntext");

                entity.Property(e => e.EventOccurrence).HasColumnType("decimal(19, 0)");

                entity.Property(e => e.EventSequence).HasColumnType("decimal(19, 0)");

                entity.Property(e => e.EventTime).HasColumnType("datetime");

                entity.Property(e => e.EventTimeUtc).HasColumnType("datetime");

                entity.Property(e => e.EventType)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.ExceptionType).HasMaxLength(256);

                entity.Property(e => e.MachineName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.Message).HasMaxLength(1024);

                entity.Property(e => e.RequestUrl).HasMaxLength(1024);
            });

            modelBuilder.Entity<CodigosDevolucion>(entity =>
            {
                entity.HasKey(e => e.IdCodigo);

                entity.ToTable("Codigos_Devolucion");

                entity.Property(e => e.IdCodigo)
                    .HasColumnName("idCodigo")
                    .ValueGeneratedNever();

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Devoluciones>(entity =>
            {
                entity.HasKey(e => new { e.FechaMovimiento, e.Serie });

                entity.Property(e => e.FechaMovimiento).HasColumnType("datetime");

                entity.Property(e => e.Serie)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EquipoInst)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Falla)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Linea)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Login)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Marca)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Modelo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreDevuelve)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsuarioRegistra)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GestionAdsl>(entity =>
            {
                entity.HasKey(e => new { e.Linea, e.TelefonoAdsl, e.CedulaCliente, e.Solicitud, e.Detalle });

                entity.ToTable("GestionADSL");

                entity.Property(e => e.Linea)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TelefonoAdsl)
                    .HasColumnName("TelefonoADSL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CedulaCliente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Solicitud)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Detalle)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CedulTecnico)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Central)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DireccionInstalacion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaDevolucion).HasColumnType("datetime");

                entity.Property(e => e.FechaRegistro).HasColumnType("datetime");

                entity.Property(e => e.FechaSolicitud)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreCliente)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Oferta)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerialEquipo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserRegistro)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GestionLb>(entity =>
            {
                entity.HasKey(e => new { e.Solicitud, e.Detalle, e.Linea, e.CedulaCliente });

                entity.ToTable("GestionLB");

                entity.Property(e => e.Solicitud)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Detalle)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Linea)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CedulaCliente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FechaDevolucion).HasColumnType("datetime");

                entity.Property(e => e.FechaRegistro).HasColumnType("datetime");

                entity.Property(e => e.FirmaCliente)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.UsuarioRegistra)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Logsito>(entity =>
            {
                entity.HasKey(e => e.FechaRepo);

                entity.ToTable("logsito");

                entity.Property(e => e.FechaRepo).HasColumnType("datetime");

                entity.Property(e => e.Texto)
                    .HasMaxLength(1024)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<NomEmpleados>(entity =>
            {
                entity.HasKey(e => e.IdEmpleado);

                entity.ToTable("NOM_EMPLEADOS");

                entity.Property(e => e.IdEmpleado)
                    .HasMaxLength(15)
                    .ValueGeneratedNever();

                entity.Property(e => e.Apellidos)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.Arp)
                    .HasColumnName("ARP")
                    .HasMaxLength(7);

                entity.Property(e => e.Barrio).HasMaxLength(50);

                entity.Property(e => e.Celular).HasMaxLength(30);

                entity.Property(e => e.Ciudad).HasMaxLength(7);

                entity.Property(e => e.Direccion).HasMaxLength(256);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(30);

                entity.Property(e => e.Eps)
                    .HasColumnName("EPS")
                    .HasMaxLength(7);

                entity.Property(e => e.EstadoCiv).HasMaxLength(7);

                entity.Property(e => e.FechaNacimiento)
                    .HasColumnName("fechaNacimiento")
                    .HasColumnType("datetime");

                entity.Property(e => e.FondoPen).HasMaxLength(7);

                entity.Property(e => e.Foto).HasColumnType("image");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.NombreConyuge).HasMaxLength(100);

                entity.Property(e => e.Sexo)
                    .IsRequired()
                    .HasMaxLength(7);

                entity.Property(e => e.TallaCam).HasMaxLength(7);

                entity.Property(e => e.TallaOve).HasMaxLength(7);

                entity.Property(e => e.TallaZap).HasMaxLength(7);

                entity.Property(e => e.Telefono).HasMaxLength(30);

                entity.Property(e => e.Tvivienda).HasMaxLength(7);
            });

            modelBuilder.Entity<NomEmpleados2>(entity =>
            {
                entity.HasKey(e => e.Identity);

                entity.ToTable("NOM_EMPLEADOS2");

                entity.Property(e => e.Identity)
                    .HasColumnName("identity")
                    .HasMaxLength(15)
                    .ValueGeneratedNever();

                entity.Property(e => e.Apellidos)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.Arp)
                    .HasColumnName("ARP")
                    .HasMaxLength(7);

                entity.Property(e => e.Barrio).HasMaxLength(50);

                entity.Property(e => e.Celular).HasMaxLength(30);

                entity.Property(e => e.Ciudad).HasMaxLength(7);

                entity.Property(e => e.Direccion).HasMaxLength(256);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(30);

                entity.Property(e => e.Eps)
                    .HasColumnName("EPS")
                    .HasMaxLength(7);

                entity.Property(e => e.EstadoCiv).HasMaxLength(7);

                entity.Property(e => e.FechaNacimiento)
                    .HasColumnName("fechaNacimiento")
                    .HasColumnType("datetime");

                entity.Property(e => e.FondoPen).HasMaxLength(7);

                entity.Property(e => e.Foto).HasColumnType("image");

                entity.Property(e => e.IdEmpleado)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.NombreConyuge).HasMaxLength(100);

                entity.Property(e => e.Sexo)
                    .IsRequired()
                    .HasMaxLength(7);

                entity.Property(e => e.TallaCam).HasMaxLength(7);

                entity.Property(e => e.TallaOve).HasMaxLength(7);

                entity.Property(e => e.TallaZap).HasMaxLength(7);

                entity.Property(e => e.Telefono).HasMaxLength(30);

                entity.Property(e => e.Tvivienda).HasMaxLength(7);
            });

            modelBuilder.Entity<NomVigencias>(entity =>
            {
                entity.HasKey(e => e.IdVigencia);

                entity.ToTable("NOM_VIGENCIAS");

                entity.Property(e => e.IdVigencia)
                    .HasColumnName("idVigencia")
                    .HasMaxLength(6)
                    .ValueGeneratedNever();

                entity.Property(e => e.Ctivo).HasColumnName("ctivo");

                entity.Property(e => e.Descripcion).HasMaxLength(15);

                entity.Property(e => e.FechaFin).HasColumnType("datetime");

                entity.Property(e => e.FechaIni).HasColumnType("datetime");
            });

            modelBuilder.Entity<TempAdslplusProdEquipos>(entity =>
            {
                entity.HasKey(e => e.Serial);

                entity.ToTable("TempADSLPLUS_PROD_EQUIPOS");

                entity.Property(e => e.Serial)
                    .HasMaxLength(25)
                    .ValueGeneratedNever();

                entity.Property(e => e.IdciudInvOrg)
                    .HasColumnName("IDCiudInvOrg")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.IdciudadInv)
                    .HasColumnName("IDCiudadInv")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Imei)
                    .HasColumnName("IMEI")
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<TempEmpleados>(entity =>
            {
                entity.HasKey(e => e.IdEmpleado);

                entity.ToTable("TEMP_EMPLEADOS");

                entity.Property(e => e.IdEmpleado)
                    .HasMaxLength(15)
                    .ValueGeneratedNever();

                entity.Property(e => e.Apellidos)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.Arp)
                    .HasColumnName("ARP")
                    .HasMaxLength(7);

                entity.Property(e => e.Barrio).HasMaxLength(50);

                entity.Property(e => e.Celular).HasMaxLength(30);

                entity.Property(e => e.Ciudad).HasMaxLength(7);

                entity.Property(e => e.Direccion).HasMaxLength(256);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(30);

                entity.Property(e => e.Eps)
                    .HasColumnName("EPS")
                    .HasMaxLength(7);

                entity.Property(e => e.EstadoCiv).HasMaxLength(7);

                entity.Property(e => e.FechaNacimiento)
                    .HasColumnName("fechaNacimiento")
                    .HasColumnType("datetime");

                entity.Property(e => e.FondoPen).HasMaxLength(7);

                entity.Property(e => e.Foto).HasColumnType("image");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.NombreConyuge).HasMaxLength(100);

                entity.Property(e => e.Sexo)
                    .IsRequired()
                    .HasMaxLength(7);

                entity.Property(e => e.TallaCam).HasMaxLength(7);

                entity.Property(e => e.TallaOve).HasMaxLength(7);

                entity.Property(e => e.TallaZap).HasMaxLength(7);

                entity.Property(e => e.Telefono).HasMaxLength(30);

                entity.Property(e => e.Tvivienda).HasMaxLength(7);
            });
        }
    }
}
