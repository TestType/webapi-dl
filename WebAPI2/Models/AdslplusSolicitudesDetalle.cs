﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusSolicitudesDetalle
    {
        public string SolicitudIdFk { get; set; }
        public string IdCodigoFk { get; set; }
        public int Item { get; set; }
        public int Cantidad { get; set; }
        public string Serial { get; set; }
        public string CantEntregada { get; set; }
        public string LineaUso { get; set; }
    }
}
