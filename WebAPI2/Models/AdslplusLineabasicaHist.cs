﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusLineabasicaHist
    {
        public string Codigoa { get; set; }
        public string Solicitud { get; set; }
        public DateTime Fecha { get; set; }
        public string HoraIni { get; set; }
        public string Tipo { get; set; }
        public string TipoProducto { get; set; }
        public string Cliente { get; set; }
        public string Identificacion { get; set; }
        public string Direccion { get; set; }
        public string Contacto { get; set; }
        public string TelContacto { get; set; }
        public string Central { get; set; }
        public string Distrito { get; set; }
        public string Liston { get; set; }
        public string ParL { get; set; }
        public string Secun { get; set; }
        public string ParS { get; set; }
        public string Pdatos { get; set; }
        public string Slot { get; set; }
        public string VpiUsuario { get; set; }
        public string VciUsuario { get; set; }
        public string Documentacion { get; set; }
        public string Vci { get; set; }
        public string IpLan { get; set; }
        public string IpLanRouter { get; set; }
        public string IpWanLocal { get; set; }
        public string IpWanRemota { get; set; }
        public string Equipo { get; set; }
        public int? NoPcs { get; set; }
        public string ReqRedLan { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public string ObservacionInst { get; set; }
        public string Detalle { get; set; }
        public string Clausula { get; set; }
        public string Impresa { get; set; }
        public string Central1 { get; set; }
        public string Coordinacion { get; set; }
        public string Terreno { get; set; }
        public string IdTecnico { get; set; }
        public string Rvisita { get; set; }
        public string Depen { get; set; }
        public string Entrega1 { get; set; }
        public string DetVoip { get; set; }
        public string Pin { get; set; }
        public string Account { get; set; }
        public string X { get; set; }
        public string Serie { get; set; }
        public string FecSol { get; set; }
        public string NDir { get; set; }
        public string MjDoc { get; set; }
        public string TDoc { get; set; }
        public string SupercomboDesmonte { get; set; }
        public string Actividad { get; set; }
        public string Barrio { get; set; }
        public string CodEstado { get; set; }
        public string CodCierre { get; set; }
        public DateTime? FechaCierre { get; set; }
        public string Ticket { get; set; }
        public string Asesor { get; set; }
        public int? Neopren { get; set; }
        public int? Alambre { get; set; }
        public int? Tensores { get; set; }
        public int? Tornillos { get; set; }
        public int? Grapas { get; set; }
        public int? Conectores { get; set; }
        public string CodCierreAdsl { get; set; }
        public string SerieEquipoInst { get; set; }
        public string SerieEquipoRet { get; set; }
        public string MarcaEquipoRet { get; set; }
        public string PrimerUso { get; set; }
        public string HoraAtencionAsesor { get; set; }
        public string Asignador { get; set; }
        public string AyudanteEtb { get; set; }
        public string SolComercial { get; set; }
        public string PlanDesmonte { get; set; }
        public int? Microfiltros { get; set; }
        public string TipoFalla { get; set; }
        public string Distritonew { get; set; }
        public string Direccion1 { get; set; }
        public string Direccion2 { get; set; }
        public string Direccion3 { get; set; }
        public int? Distancia { get; set; }
        public string ResultadoVisita { get; set; }
        public string PersonaRecibe { get; set; }
        public string Pvoz { get; set; }
        public DateTime FechaHoraReactiva { get; set; }
        public string ImagenGpnba { get; set; }
        public string EstadoContrato { get; set; }
    }
}
