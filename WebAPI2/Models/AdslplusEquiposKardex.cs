﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusEquiposKardex
    {
        public DateTime FechaMovimiento { get; set; }
        public string SerialEquipo { get; set; }
        public string Bodega { get; set; }
        public string TipoMovimiento { get; set; }
        public string SolicitudLogistica { get; set; }
        public string ObservacionesMov { get; set; }
        public Guid? UsrRegistraMov { get; set; }
        public string Linea { get; set; }
        public string TipoTrabajo { get; set; }
        public string Boleta { get; set; }
        public DateTime? FechaEntrada { get; set; }
    }
}
