﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusCodigosAprov
    {
        public string IdCodigo { get; set; }
        public int TipoCodigo { get; set; }
        public string DescripcionCodigo { get; set; }
        public string Origen { get; set; }
        public string Naturaleza { get; set; }
        public bool? Activo { get; set; }
        public bool? RedInterna { get; set; }
    }
}
