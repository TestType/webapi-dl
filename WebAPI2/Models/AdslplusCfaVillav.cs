﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusCfaVillav
    {
        public int IdCfa { get; set; }
        public string Cfa { get; set; }
        public string CfaDesc { get; set; }
        public string CodRta { get; set; }
        public bool? Activo { get; set; }
    }
}
