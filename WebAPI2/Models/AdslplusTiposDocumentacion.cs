﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusTiposDocumentacion
    {
        public AdslplusTiposDocumentacion()
        {
            AdslplusDocumentacion = new HashSet<AdslplusDocumentacion>();
            AdslplusTiposOferta = new HashSet<AdslplusTiposOferta>();
        }

        public int IdTipoDocumentacion { get; set; }
        public string Nombre { get; set; }

        public ICollection<AdslplusDocumentacion> AdslplusDocumentacion { get; set; }
        public ICollection<AdslplusTiposOferta> AdslplusTiposOferta { get; set; }
    }
}
