﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusTiposCausaDevolucion
    {
        public AdslplusTiposCausaDevolucion()
        {
            AdslplusDocumentacion = new HashSet<AdslplusDocumentacion>();
            AdslplusDocumentacionHistorico = new HashSet<AdslplusDocumentacionHistorico>();
        }

        public int IdTipoCausaDevolucion { get; set; }
        public string Nombre { get; set; }
        public bool? Activo { get; set; }

        public ICollection<AdslplusDocumentacion> AdslplusDocumentacion { get; set; }
        public ICollection<AdslplusDocumentacionHistorico> AdslplusDocumentacionHistorico { get; set; }
    }
}
