﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusAsegAgendas
    {
        public string LineaFk { get; set; }
        public DateTime FechaAgendaFk { get; set; }
        public int CtivoAgenda { get; set; }
        public DateTime? FechaUltimaAgenda { get; set; }
        public string IdTecnicoFk { get; set; }
        public string Observaciones { get; set; }
        public string Tipo { get; set; }
        public DateTime? FechaHoraRegistro { get; set; }
        public Guid? UsrRegistro { get; set; }
        public DateTime? FechaFinRegistro { get; set; }
        public Guid? UsrFinRegistro { get; set; }
        public DateTime? FechaHoraRegistro2 { get; set; }

        public AdslplusAseguramiento AdslplusAseguramiento { get; set; }
        public AdslplusTecnicos IdTecnicoFkNavigation { get; set; }
    }
}
