﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusLogSolicitudes
    {
        public string SolicitudId { get; set; }
        public DateTime FechaSolicitud { get; set; }
        public Guid? UsrRegistra { get; set; }
    }
}
