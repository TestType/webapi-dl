﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusTiposOferta
    {
        public string TipoServicio { get; set; }
        public string TipoOferta { get; set; }
        public int IdTipoDocumentacion { get; set; }

        public AdslplusTiposDocumentacion IdTipoDocumentacionNavigation { get; set; }
    }
}
