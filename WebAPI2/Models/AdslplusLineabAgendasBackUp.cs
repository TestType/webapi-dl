﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusLineabAgendasBackUp
    {
        public string SolicitudFk { get; set; }
        public DateTime FechaFk { get; set; }
        public string DetalleFk { get; set; }
        public int CtivoAgenda { get; set; }
        public DateTime FechaUltimaAgenda { get; set; }
        public string IdTecnicoFk { get; set; }
        public string Observaciones { get; set; }
        public int? Tipo { get; set; }
        public DateTime? FechaHoraRegistro { get; set; }
        public Guid? UsrRegistro { get; set; }
        public DateTime? FechaFinRegistro { get; set; }
        public Guid? UsrFinRegistro { get; set; }
        public string EstadoContrato { get; set; }
        public DateTime? FechaHoraRegistro2 { get; set; }
    }
}
