﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusLogActualizacionEquipos
    {
        public DateTime FechaHoraRegistro { get; set; }
        public string SerialOriginal { get; set; }
        public string SerialNuevo { get; set; }
        public string Linea { get; set; }
        public string Fecha { get; set; }
        public Guid? UsuarioCambio { get; set; }
    }
}
