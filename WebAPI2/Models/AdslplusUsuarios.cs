﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusUsuarios
    {
        public Guid UserIdFk { get; set; }
        public string IdEmpleadoFk { get; set; }
        public string UserName { get; set; }
        public int? ZonaFk { get; set; }
    }
}
