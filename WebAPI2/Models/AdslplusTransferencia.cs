﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusTransferencia
    {
        public AdslplusTransferencia()
        {
            AdslplusDocumentacion = new HashSet<AdslplusDocumentacion>();
            AdslplusDocumentacionHistorico = new HashSet<AdslplusDocumentacionHistorico>();
            AdslplusTransferenciaDetalle = new HashSet<AdslplusTransferenciaDetalle>();
        }

        public int IdTransferencia { get; set; }
        public int Anio { get; set; }
        public int Consecutivo { get; set; }
        public Guid UserId { get; set; }
        public bool Finalizada { get; set; }
        public DateTime Fecha { get; set; }

        public ICollection<AdslplusDocumentacion> AdslplusDocumentacion { get; set; }
        public ICollection<AdslplusDocumentacionHistorico> AdslplusDocumentacionHistorico { get; set; }
        public ICollection<AdslplusTransferenciaDetalle> AdslplusTransferenciaDetalle { get; set; }
    }
}
