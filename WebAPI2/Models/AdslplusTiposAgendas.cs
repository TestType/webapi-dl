﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusTiposAgendas
    {
        public int IdTipoAgenda { get; set; }
        public string TpoAgenda { get; set; }
    }
}
