﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusUsuariosCoordinaciones
    {
        public Guid UserId { get; set; }
        public string Coordinacion { get; set; }

        public AspnetUsers User { get; set; }
    }
}
