﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusPqrs
    {
        public string Linea { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string NumSolicitud { get; set; }
        public int? TipologiaDp { get; set; }
        public string DescripcionDp { get; set; }
        public string NumDp { get; set; }
        public string Oferta { get; set; }
        public DateTime? FechaAgenda { get; set; }
        public string Aplica { get; set; }
        public string Respuesta { get; set; }
        public Guid? UsrRegistro { get; set; }
        public string Estado { get; set; }
        public Guid? UsrCierre { get; set; }
        public DateTime? FechaCierre { get; set; }
        public string CedulaCliente { get; set; }
        public string NombreCliente { get; set; }
        public string Ticket { get; set; }
        public string Asesor { get; set; }
        public string LoginTecnico { get; set; }
        public string Central { get; set; }
        public string Distrito { get; set; }
        public string Liston { get; set; }
        public string PosDatos { get; set; }
        public string Pvoz { get; set; }
        public string ParL { get; set; }
        public string Secundaria { get; set; }
        public string ParS { get; set; }
    }
}
