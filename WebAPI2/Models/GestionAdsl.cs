﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class GestionAdsl
    {
        public string Linea { get; set; }
        public string FechaSolicitud { get; set; }
        public string DireccionInstalacion { get; set; }
        public string TelefonoAdsl { get; set; }
        public string NombreCliente { get; set; }
        public string CedulaCliente { get; set; }
        public string SerialEquipo { get; set; }
        public string Oferta { get; set; }
        public string CedulTecnico { get; set; }
        public string Solicitud { get; set; }
        public string Detalle { get; set; }
        public string UserRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string Estado { get; set; }
        public DateTime? FechaDevolucion { get; set; }
        public string Central { get; set; }
    }
}
