﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusMaterialesKardex
    {
        public DateTime FechaMovimiento { get; set; }
        public string CodigoMaterial { get; set; }
        public string Bodega { get; set; }
        public string TipoMovimiento { get; set; }
        public decimal? CantidadMaterial { get; set; }
        public string SolicitudLogistica { get; set; }
        public string ObservacionesMov { get; set; }
        public Guid? UsrRegistraMov { get; set; }
        public string Linea { get; set; }
        public DateTime? Fecha { get; set; }
        public string BoletaIngreso { get; set; }
        public string TipoTrabajo { get; set; }
        public int? EstadoMaterial { get; set; }
    }
}
