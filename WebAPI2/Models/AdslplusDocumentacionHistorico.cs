﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusDocumentacionHistorico
    {
        public int IdDocumentacionHistorico { get; set; }
        public int IdDocumentacion { get; set; }
        public Guid UserId { get; set; }
        public DateTime Fecha { get; set; }
        public int IdEstadoDocumentacion { get; set; }
        public int? IdTipoNovedad { get; set; }
        public int? IdTipoCausaDevolucion { get; set; }
        public int? CantidadFolios { get; set; }
        public int? IdTransferencia { get; set; }
        public bool? Auditado { get; set; }

        public AdslplusDocumentacion IdDocumentacionNavigation { get; set; }
        public AdslplusEstadosDocumentacion IdEstadoDocumentacionNavigation { get; set; }
        public AdslplusTiposCausaDevolucion IdTipoCausaDevolucionNavigation { get; set; }
        public AdslplusTiposNovedad IdTipoNovedadNavigation { get; set; }
        public AdslplusTransferencia IdTransferenciaNavigation { get; set; }
        public AspnetUsers User { get; set; }
    }
}
