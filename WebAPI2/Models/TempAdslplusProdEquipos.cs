﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class TempAdslplusProdEquipos
    {
        public string Serial { get; set; }
        public string Imei { get; set; }
        public int CodTipEquipo { get; set; }
        public int CodTipUso { get; set; }
        public int CodTipEstado { get; set; }
        public string IdciudadInv { get; set; }
        public string IdciudInvOrg { get; set; }
    }
}
