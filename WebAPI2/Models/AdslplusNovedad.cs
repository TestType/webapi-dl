﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusNovedad
    {
        public int IdNovedad { get; set; }
        public string Solicitud { get; set; }
        public string Linea { get; set; }
        public string Novedad { get; set; }
        public DateTime? Fecharegistro { get; set; }
        public string UsuarioRegistro { get; set; }
        public DateTime? FechaSolicitud { get; set; }
    }
}
