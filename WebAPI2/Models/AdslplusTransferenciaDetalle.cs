﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusTransferenciaDetalle
    {
        public int IdTransferencia { get; set; }
        public int IdDocumentacion { get; set; }
        public bool? Chequeado { get; set; }

        public AdslplusDocumentacion IdDocumentacionNavigation { get; set; }
        public AdslplusTransferencia IdTransferenciaNavigation { get; set; }
    }
}
