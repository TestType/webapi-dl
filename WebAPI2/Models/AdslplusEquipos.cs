﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusEquipos
    {
        public string Serial { get; set; }
        public string Codigo { get; set; }
        public string Boleta { get; set; }
        public DateTime? FechaIngreso { get; set; }
        public DateTime? FechaScaneo { get; set; }
        public Guid? UsrScaneo { get; set; }
        public string EstadoEquipo { get; set; }
        public string BodegaIn { get; set; }

        public AdslplusCodigosLogistica CodigoNavigation { get; set; }
    }
}
