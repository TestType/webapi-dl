﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusLogUsuarios
    {
        public string UserName { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string Evento { get; set; }
        public Guid? UsuarioEvento { get; set; }
        public string Justificacion { get; set; }
    }
}
