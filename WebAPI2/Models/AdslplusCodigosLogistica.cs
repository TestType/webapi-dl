﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusCodigosLogistica
    {
        public AdslplusCodigosLogistica()
        {
            AdslplusEquipos = new HashSet<AdslplusEquipos>();
        }

        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string UnidadMedida { get; set; }
        public bool? UsoAprov { get; set; }
        public bool? UsoAseg { get; set; }
        public bool? UsoAdsl { get; set; }
        public bool? UsoEmpalmeria { get; set; }
        public int? Tipo { get; set; }

        public ICollection<AdslplusEquipos> AdslplusEquipos { get; set; }
    }
}
