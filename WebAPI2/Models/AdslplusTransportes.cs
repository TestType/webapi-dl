﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusTransportes
    {
        public string LineaFk { get; set; }
        public string SolicitudFk { get; set; }
        public DateTime FechaAgendaFk { get; set; }
        public int CtivoSolicitud { get; set; }
        public string IdTecnicoFk { get; set; }
        public int? TipoTramite { get; set; }
        public string LineaDestino { get; set; }
        public string SolicitudDestino { get; set; }
        public DateTime? FechaDestino { get; set; }
        public Guid? UsrRegistro { get; set; }
        public DateTime? FechaHoraRegistro { get; set; }
        public string Estado { get; set; }
        public string Observaciones { get; set; }
        public string DireccionDestino { get; set; }
        public string CentralDestino { get; set; }
        public int? ResultadoAsignacion { get; set; }
        public DateTime? FechaHoraAtencion { get; set; }
        public string PlacaAsignada { get; set; }
        public Guid? UsrAsignador { get; set; }
        public string Ticket { get; set; }
        public string ObservacionesAsig { get; set; }
    }
}
