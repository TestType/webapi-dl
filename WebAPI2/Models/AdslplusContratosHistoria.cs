﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusContratosHistoria
    {
        public string NumContrato { get; set; }
        public DateTime FechaEstado { get; set; }
        public string Estado { get; set; }
        public Guid? UsrEstado { get; set; }
    }
}
