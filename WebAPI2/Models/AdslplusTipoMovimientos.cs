﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusTipoMovimientos
    {
        public int CodTipMov { get; set; }
        public string DesTipMov { get; set; }
    }
}
