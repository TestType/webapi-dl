﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusProdMaterial
    {
        public string Boleta { get; set; }
        public int CodTipMaterial { get; set; }
        public string UnidadMedida { get; set; }
        public int Cantidad { get; set; }
        public int CodTipUso { get; set; }
        public int CodTipEstado { get; set; }
    }
}
