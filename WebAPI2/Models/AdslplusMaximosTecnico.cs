﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusMaximosTecnico
    {
        public string NombreElemento { get; set; }
        public int? Maximo { get; set; }
    }
}
