﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusFallasCpe
    {
        public int IdCodFalla { get; set; }
        public string DescripcionFalla { get; set; }
        public bool? Activo { get; set; }
    }
}
