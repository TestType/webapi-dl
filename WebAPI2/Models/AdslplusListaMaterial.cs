﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusListaMaterial
    {
        public int CodTipMaterial { get; set; }
        public string DesTipMateriall { get; set; }
        public string UnidadMedida { get; set; }
        public int? UsoAdsl { get; set; }
        public int? UsoEmpalmeria { get; set; }
        public int? LineaUso { get; set; }
    }
}
