﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusSolicitudHistoria
    {
        public string IdSolicitud { get; set; }
        public string Estado { get; set; }
        public DateTime FechaEstado { get; set; }
        public Guid UsrId { get; set; }
    }
}
