﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class CodigosDevolucion
    {
        public int IdCodigo { get; set; }
        public string Descripcion { get; set; }
    }
}
