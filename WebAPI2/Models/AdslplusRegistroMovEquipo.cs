﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusRegistroMovEquipo
    {
        public string Boleta { get; set; }
        public string IdEquipo { get; set; }
        public int CodTipMov { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime? FechaVencimiento { get; set; }
        public int CcSolicitante { get; set; }
        public string IdBodCiudadInicial { get; set; }
        public string IdBodCiudadFinal { get; set; }
        public string IdBodZonaInicial { get; set; }
        public string IdBodZonaFinal { get; set; }
        public int? IdTecnicoInicial { get; set; }
        public int? IdTecnicoFinal { get; set; }
        public string NFomrImf04 { get; set; }
        public string ValeAcompañamiento { get; set; }
        public string CodColvatel { get; set; }
        public Guid? UsrRegistra { get; set; }
        public int? CodTipEquipo { get; set; }
        public int? CodTipUso { get; set; }
    }
}
