﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class GestionLb
    {
        public int Oferta { get; set; }
        public int FechaInstalacion { get; set; }
        public string Solicitud { get; set; }
        public string Detalle { get; set; }
        public int Nombre { get; set; }
        public int CedulaTecnico { get; set; }
        public int DireccionInstalacion { get; set; }
        public int Central { get; set; }
        public int Distrito { get; set; }
        public int Liston { get; set; }
        public int Caja { get; set; }
        public string Linea { get; set; }
        public int Alambre { get; set; }
        public string CedulaCliente { get; set; }
        public string UsuarioRegistra { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string Estado { get; set; }
        public string FirmaCliente { get; set; }
        public int ParC { get; set; }
        public DateTime? FechaDevolucion { get; set; }
    }
}
