﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusMarcasEquipos
    {
        public string SerieId { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int? Condicion { get; set; }
    }
}
