﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusSolicitudes
    {
        public string SolicitudId { get; set; }
        public Guid UsuarioUid { get; set; }
        public DateTime FechaSolicitud { get; set; }
        public string TipoSolicitud { get; set; }
        public string CedulaEmpleado { get; set; }
        public string Observaciones { get; set; }
        public DateTime? FechaEstado { get; set; }
        public string Estado { get; set; }
        public string CentroCostosFk { get; set; }
        public string ZonaFk { get; set; }
        public Guid? Uidestado { get; set; }
        public string MotivoAnulacion { get; set; }
        public int? CodTipoUso { get; set; }
    }
}
