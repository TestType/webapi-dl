﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusLfaVillav
    {
        public int IdLfa { get; set; }
        public string Lfa { get; set; }
        public string LfaDesc { get; set; }
        public string CodRta { get; set; }
        public string CodCfa { get; set; }
        public bool? Activo { get; set; }
    }
}
