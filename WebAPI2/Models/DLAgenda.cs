﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI2.Models
{
    public class DLAgenda
    {
        public string Linea { get; set; }
        public string Solicitud { get; set; }
        public DateTime FechaAgenda { get; set; }
        public int? Consecutivo { get; set; }
    }
}
