﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusDocumentacion
    {
        public AdslplusDocumentacion()
        {
            AdslplusDocumentacionHistorico = new HashSet<AdslplusDocumentacionHistorico>();
            AdslplusTransferenciaDetalle = new HashSet<AdslplusTransferenciaDetalle>();
        }

        public int IdDocumentacion { get; set; }
        public string AprovisionamientoLinea { get; set; }
        public string AprovisionamientoSolicitud { get; set; }
        public DateTime? AprovisionamientoFechaAgenda { get; set; }
        public string LineaBasicaDetalle { get; set; }
        public string LineaBasicaSolicitud { get; set; }
        public DateTime? LineaBasicaFecha { get; set; }
        public string LineaCorrecta { get; set; }
        public string SolicitudCorrecta { get; set; }
        public int? IdTipoDocumentacion { get; set; }
        public int? CantidadFolios { get; set; }
        public int? IdEstadoDocumentacion { get; set; }
        public int? IdTipoNovedad { get; set; }
        public int? IdTipoCausaDevolucion { get; set; }
        public int? IdTransferencia { get; set; }
        public bool? Auditado { get; set; }

        public AdslplusAprovisionamiento Aprovisionamiento { get; set; }
        public AdslplusEstadosDocumentacion IdEstadoDocumentacionNavigation { get; set; }
        public AdslplusTiposCausaDevolucion IdTipoCausaDevolucionNavigation { get; set; }
        public AdslplusTiposDocumentacion IdTipoDocumentacionNavigation { get; set; }
        public AdslplusTiposNovedad IdTipoNovedadNavigation { get; set; }
        public AdslplusTransferencia IdTransferenciaNavigation { get; set; }
        public AdslplusLineabasica LineaBasica { get; set; }
        public ICollection<AdslplusDocumentacionHistorico> AdslplusDocumentacionHistorico { get; set; }
        public ICollection<AdslplusTransferenciaDetalle> AdslplusTransferenciaDetalle { get; set; }
    }
}
