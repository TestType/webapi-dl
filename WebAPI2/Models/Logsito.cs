﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class Logsito
    {
        public DateTime FechaRepo { get; set; }
        public string Texto { get; set; }
    }
}
