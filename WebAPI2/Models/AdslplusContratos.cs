﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusContratos
    {
        public string NumContrato { get; set; }
        public string IdTecnicoFk { get; set; }
        public bool? EstadoContrato { get; set; }
        public bool? RevFirma { get; set; }
        public bool? RevNombre { get; set; }
        public bool? RevCedula { get; set; }
        public Guid? UsrRecibe { get; set; }
        public DateTime? FechaRecibe { get; set; }
    }
}
