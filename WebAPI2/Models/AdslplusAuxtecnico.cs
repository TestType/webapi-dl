﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusAuxtecnico
    {
        public int IdTecnicoAux { get; set; }
        public string Identificacion { get; set; }
        public string Nombre { get; set; }
        public int? Codigo { get; set; }
        public string Cargo { get; set; }
        public string Contratante { get; set; }
        public bool? Estado { get; set; }
        public DateTime? FechaRegistro { get; set; }
    }
}
