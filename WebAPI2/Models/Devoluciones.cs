﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class Devoluciones
    {
        public DateTime FechaMovimiento { get; set; }
        public string Login { get; set; }
        public int? Items { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Serie { get; set; }
        public int? Usb { get; set; }
        public int? Adaptador { get; set; }
        public string Falla { get; set; }
        public string EquipoInst { get; set; }
        public string Linea { get; set; }
        public string Tipo { get; set; }
        public string NombreDevuelve { get; set; }
        public string UsuarioRegistra { get; set; }
    }
}
