﻿using System;
using System.Collections.Generic;

namespace WebAPI2.Models
{
    public partial class AdslplusParameventos
    {
        public int IdParametro { get; set; }
        public string VarProducto { get; set; }
        public int? IntCantminEnFranja { get; set; }
        public double? FloValMenEnfranja { get; set; }
        public double? FloValMayEnFranja { get; set; }
        public int? IntCantminFueraFranja { get; set; }
        public double? FloValMenFueraFranja { get; set; }
        public double? FloValMayFueraFranja { get; set; }
        public DateTime? DatFechaRegistro { get; set; }
        public string VarUsuarioRegistra { get; set; }
        public int? SobreAgenda { get; set; }
    }
}
