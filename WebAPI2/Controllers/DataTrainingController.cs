﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebAPI2.Models;

namespace WebAPI2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DataTrainingController : ControllerBase
    {
        private readonly ADSLplusContext _context;
        public DataTrainingController(ADSLplusContext context)
        {
            _context = context;
        }

        // GET: api/DataTraining
        [HttpGet]
        public ActionResult Get(string Filtro, string FechaIni = "", string FechaFin = "", string oferta = "")
        {
            DateTime fechaini = DateTime.Parse(FechaIni);
            DateTime fechafin = DateTime.Parse(FechaFin);
            switch (Filtro)
            {
                case "DatosTest":
                    return Ok(ConsultarAgendaHistorica(fechaini, fechafin));
                case "DatosHistoricos":
                    return Ok(ConsultarActividadesHistorico(fechaini, fechafin));
                case "DatosPorSemanas":
                    return Ok(ConsultarCantidadActividadesPorSemana(fechaini, fechafin));
                case "DatosPorDias":
                    return Ok(ConsultarCantidadActividadesPorDia(fechaini, fechafin, oferta));
                case "DatosPorActividad":
                    return Ok(ConsultarCantidadActividadesPorActividad(fechaini, fechafin));
                default:
                    var data = _context.AdslplusAprovAgendas.Take(10).ToList();
                    return Ok(data);
            }
        }

        public List<DLAgenda> ConsultarAgendaHistorica(DateTime FechaIni, DateTime FechaFin)
        {
            List<DLAgenda> ListaConsecutivos = _context.AdslplusAprovAgendas
                .Where(x => x.FechaAgendaFk >= FechaIni && x.FechaAgendaFk <= FechaFin)
                .GroupBy(x => new { x.LineaFk, x.SolicitudFk, x.FechaAgendaFk })
                .Select(x => new DLAgenda
                {
                    Linea = x.Key.LineaFk.ToString(),
                    Solicitud = x.Key.SolicitudFk.ToString(),
                    FechaAgenda = x.Key.FechaAgendaFk,
                    Consecutivo = x.Max(c => c.CtivoAgenda)
                }).ToList();

            return ListaConsecutivos;
        }

        public List<DLActividades> ConsultarActividadesHistorico(DateTime FechaIni, DateTime FechaFin, string oferta = "") {
            List<DLActividades> listaactividades = new List<DLActividades>();

            listaactividades.AddRange(_context.AdslplusAprovisionamiento
                .Where(x => x.CodEstado.Contains("RDY") 
                    && x.FechaAgenda >= FechaIni && x.FechaAgenda <= FechaFin
                    && (oferta != null ? x.Oferta.Contains(oferta) : true))
                .Select(x => new DLActividades {
                    Linea = x.Linea,
                    Solicitud = x.Solicitud,
                    FechaAgenda = x.FechaAgenda,
                    FechaCierre = x.FechaCierre,
                    Oferta = x.Oferta,
                    TipoProducto = x.TipoProducto,
                    Estado = x.CodEstado,
                    CodigoCierre = x.CodigoCierre,
                    IdTecnico = _context.AdslplusAprovAgendas
                        .Where(i => i.LineaFk ==x.Linea && i.SolicitudFk == x.Solicitud && i.FechaAgendaFk == x.FechaAgenda)
                        .OrderBy(i => i.CtivoAgenda)
                        .Select(i => i.IdTecnicoFk)
                        .LastOrDefault()
                }).ToList());

            listaactividades.AddRange(_context.AdslplusLineabasica
                .Where(x => x.CodEstado.Contains("RDY")
                    && x.Fecha >= FechaIni && x.Fecha <= FechaFin
                    && (oferta != null ? x.Tipo.Contains(oferta) : true))
                .Select(x => new DLActividades
                {
                    Linea = x.Codigoa,
                    Solicitud = x.Solicitud,
                    FechaAgenda = x.Fecha,
                    FechaCierre = x.FechaCierre,
                    Oferta = x.Tipo,
                    TipoProducto = x.TipoProducto,
                    Estado = x.CodEstado,
                    CodigoCierre = x.CodCierre,
                    IdTecnico = x.IdTecnico.ToString() ?? ""
                }));

            return listaactividades;            
        }

        public dynamic ConsultarCantidadActividadesPorSemana(DateTime FechaIni, DateTime FechaFin) {
            List<DLActividades> listaactividades = ConsultarActividadesHistorico(FechaIni,FechaFin);

            var lstagrupamiento = listaactividades
                .GroupBy(x => new {Fecha = x.FechaAgenda.StartOfWeek(DayOfWeek.Monday) })
                .Select(x => new { x.Key.Fecha, Cantidad = x.Count() })
                .OrderBy(x => x.Fecha)
                .ToList();

            return lstagrupamiento;
        }

        public dynamic ConsultarCantidadActividadesPorDia(DateTime FechaIni, DateTime FechaFin, string oferta)
        {
            List<DLActividades> listaactividades = ConsultarActividadesHistorico(FechaIni, FechaFin, oferta);

            var lstagrupamiento = listaactividades
                .GroupBy(x => new { Fecha = x.FechaAgenda.Date })
                .Select(x => new {
                    x.Key.Fecha,
                    cantidad_servicios = x.Count(),
                    cantidad_tecnicos = x.Select(c => c.IdTecnico).Distinct().Count() })
                .OrderBy(x => x.Fecha)
                .ToList();

            return lstagrupamiento;
        }

        public dynamic ConsultarCantidadActividadesPorActividad(DateTime FechaIni, DateTime FechaFin) {
            List<DLActividades> listaactividades = ConsultarActividadesHistorico(FechaIni, FechaFin);

            var lstAgrupamiento = listaactividades
                .GroupBy(x => new { Fecha = x.FechaAgenda.Date, x.Oferta })
                .Select(x => new {
                    x.Key.Fecha,
                    x.Key.Oferta,
                    cantidad_servicios = x.Count(),
                    cantidad_tecnicos = x.Select(c => c.IdTecnico).Distinct().Count() })
                .OrderBy(x => x.Fecha)
                .ToList();

            return lstAgrupamiento;
        }

    }

}

public static class DateTimeExtensions
{
    public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
    {
        int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
        return dt.AddDays(-1 * diff).Date;
    }
}